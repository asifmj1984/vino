package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MaridajesActivity extends AppCompatActivity {
    Button btnPescado, btnAve, btnQuesos, btnTernera, btnCordero, btnCerdo, btnEntrantes, btnPostres,
    btnEnsalada, btnPasta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maridajes);

        btnPescado = findViewById(R.id.btnPescado);
        btnAve = findViewById(R.id.btnAve);
        btnQuesos = findViewById(R.id.btnQueso);
        btnTernera =  findViewById(R.id.btnTernera);
        btnCordero = findViewById(R.id.btnCordero);
        btnCerdo =  findViewById(R.id.btnCerdo);
        btnEntrantes = findViewById(R.id.btnEntrantes);
        btnPostres =  findViewById(R.id.btnPostres);
        btnEnsalada = findViewById(R.id.btnEnsalada);
        btnPasta =  findViewById(R.id.btnPasta);

        goToIndicatedPage(btnPescado,getApplicationContext().getString(R.string.pescado));
        goToIndicatedPage(btnAve, getApplicationContext().getString(R.string.pollo));
        goToIndicatedPage(btnQuesos, getApplicationContext().getString(R.string.queso));
        goToIndicatedPage(btnTernera, getApplicationContext().getString(R.string.ternera));
        goToIndicatedPage(btnCordero, getApplicationContext().getString(R.string.cordero));
        goToIndicatedPage(btnCerdo, getApplicationContext().getString(R.string.cerdo));
        goToIndicatedPage(btnEntrantes, getApplicationContext().getString(R.string.entrantes));
        goToIndicatedPage(btnPostres, getApplicationContext().getString(R.string.postre));
        goToIndicatedPage(btnEnsalada, getApplicationContext().getString(R.string.ensaladas));
        goToIndicatedPage(btnPasta, getApplicationContext().getString(R.string.pasta));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menhome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home1:
                Intent mainActivity = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(mainActivity);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void goToIndicatedPage(Button btn, final String tipoVino){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getApplicationContext(), MaridajeActivity.class);
                mainActivity.putExtra("tipo", tipoVino);
                startActivity(mainActivity);
            }
        });
    }
}
