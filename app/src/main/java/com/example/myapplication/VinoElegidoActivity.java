package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Controladores.RecyclerTouchListener;
import com.example.myapplication.Controladores.VinosAdapter;
import com.example.myapplication.Modelos.Vino;
import com.mysql.jdbc.Connection;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class VinoElegidoActivity extends AppCompatActivity {
    String datoRecibido2;
    String datoRecibido1;
    String precio1;
    String precio2;
    RecyclerView rvVinos;
    VinosAdapter vinosAdapter;
    ArrayList<Vino> vinos = new ArrayList<>();
    Button verDetalleVino;
    ArrayList<String> busquedas = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vino_elegido);
        datoRecibido2 = getIntent().getExtras().getString("datoRecibido2");
        datoRecibido1 = getIntent().getExtras().getString("datoRecibido1");
        precio1 = getIntent().getExtras().getString("precio1");
        precio2 = getIntent().getExtras().getString("precio2");
        GetVinos task = new GetVinos();
        task.execute("");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dos, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        GetVinos2 task = new GetVinos2();
        switch (item.getItemId()) {
            case R.id.action_settings2:
                task.execute("nombre");
                return true;
            case R.id.action_buscarPorBodega2:
                GetNombreBodegaVino task2 = new GetNombreBodegaVino();
                task2.execute("bodega");
                return true;
            case R.id.action_buscarPorAnio2:
                task.execute("anyada");
                return true;
            case R.id.action_buscarPorDenomicacion2:
                task.execute("denomicacionDeOrigen");
                return true;
            case R.id.action_buscarPorAlcohol2:
                task.execute("alcohol");
                return true;
            case R.id.action_buscarPorVariante2:
                task.execute("variantesDeUva");
                return true;
            case R.id.action_buscarPorPrecio2:
                task.execute("precio");
                return true;
            case R.id.action_buscarEntrePrecios2:
                rellenarDatosDePrecio();
                return true;

            case R.id.homeVino2:
                Intent mainActivity = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(mainActivity);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public class GetVinos extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            com.mysql.jdbc.Connection con;

            Vino vino = null;
            int id = 0;
            String nombre = null;
            String imagen = null;
            String anyada = null;
            Double precio = null;
            java.sql.Statement st;
            ResultSet rs;
            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                if(datoRecibido1.equals("bodega")){
                    rs = st.executeQuery("Select v.*  from vinos v inner join bodegas b on v.bodega = b.id  where b.nombre = '" + datoRecibido2 +"'");
                    System.out.println("Select *  from vinos where "+datoRecibido1+" = '" + datoRecibido2 +"' order by v.nombre");
                    while (rs.next()) {
                        id = rs.getInt("id");
                        nombre = rs.getString("nombre");
                        imagen = rs.getString("imagen");
                        anyada = rs.getString("anyada");
                        precio = rs.getDouble("precio");
                        vino = new Vino(id, nombre, imagen, anyada, precio);
                        vinos.add(vino);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }
                }else{
                    if(precio1 != null){
                        rs = st.executeQuery("Select *  from vinos where precio between  '"+precio1+"' AND '" + precio2 +"'");
                        System.out.println("Select *  from vinos where precio between   "+precio1+" AND '" + precio2 +"' order by v.nombre");
                        while (rs.next()) {
                            id = rs.getInt("id");
                            nombre = rs.getString("nombre");
                            imagen = rs.getString("imagen");
                            anyada = rs.getString("anyada");
                            precio = rs.getDouble("precio");
                            vino = new Vino(id, nombre, imagen, anyada, precio);
                            vinos.add(vino);
                            //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                        }
                    }else{
                        rs = st.executeQuery("Select *  from vinos where "+datoRecibido1+" = '" + datoRecibido2 +"'");
                        System.out.println("Select *  from vinos where "+datoRecibido1+" = '" + datoRecibido2 +"'");
                        while (rs.next()) {
                            id = rs.getInt("id");
                            nombre = rs.getString("nombre");
                            imagen = rs.getString("imagen");
                            anyada = rs.getString("anyada");
                            precio = rs.getDouble("precio");
                            vino = new Vino(id, nombre, imagen, anyada, precio);
                            vinos.add(vino);
                            //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                        }
                    }

                }





            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setUI();
        }
    }

    //*** SE INICIAN LAS VARIABLES Y EL RECYCLER ***
    public void setUI() {

        rvVinos = findViewById(R.id.rvVinoElegido);

        vinosAdapter = new VinosAdapter(vinos, this);

        GridLayoutManager mLayoutManager =   new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.VERTICAL, false);

        rvVinos.setLayoutManager(mLayoutManager);;

        //rvVinos.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        rvVinos.setAdapter(vinosAdapter);

        rvVinos.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvVinos, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                verDetalleVino = view.findViewById(R.id.btnVerDetallesVino);

                verDetalleVino.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), VinoDetalladoActivity.class);
                        intent.putExtra("idVino", vinos.get(position).getId());
                        startActivity(intent);
                    }
                });


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        vinosAdapter.notifyDataSetChanged();


    }

    private boolean etEstaVacio(EditText et){
        boolean comprobador;

        if(et.getText().toString().equals("")){
            comprobador= true;
            et.setBackgroundColor(getResources().getColor(R.color.rojopeligro));
        }else {
            comprobador = false;
            et.setBackgroundColor(getResources().getColor(R.color.whiteFF));
        }

        return  comprobador;
    }
    private void rellenarDatosDePrecio(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(VinoElegidoActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.dialog_buscar_entre_precios, null);
        builder.setView(v);
        final TextView tvTitulo = v.findViewById(R.id.tvDeBusquedaEntrePrecios);
        tvTitulo.setText(getApplicationContext().getText(R.string.elijamin));
        final Button btnAceptar = v.findViewById(R.id.btnAceptartEntrePrecios);
        final Button btncancelar = v.findViewById(R.id.btnCancelarEntrePrecios);
        final EditText minPrecio = v.findViewById(R.id.etMin);
        final EditText maxPrecio = v.findViewById(R.id.etMax);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(etEstaVacio(minPrecio)) && !(etEstaVacio(maxPrecio))){
                    Intent mainActivity = new Intent(getApplicationContext(), VinoElegidoActivity.class);

                    mainActivity.putExtra("precio1", minPrecio.getText().toString());
                    mainActivity.putExtra("precio2", maxPrecio.getText().toString());
                    System.out.println("precios                      " + minPrecio.getText().toString() +"        "+maxPrecio.getText().toString());
                    mainActivity.putExtra("datoRecibido1", "");
                    startActivity(mainActivity);
                    alertDialog[0].dismiss();
                }

            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    private void rellenarDatosDeBusqueda(final String mensaje2){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(VinoElegidoActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.dialog_buscar_por_provincia, null);
        builder.setView(v);
        final TextView tvTitulo = v.findViewById(R.id.tvDeBusqueda);
        tvTitulo.setText(getApplicationContext().getText(R.string.elija));
        final Button btnAceptar = v.findViewById(R.id.btnAceptartProvincia);
        final Button btncancelar = v.findViewById(R.id.btnCancelarProvincia);
        final Spinner spinnerProvincias = v.findViewById(R.id.spinnerProvincias);
        String [] arrayProvincias  = new String[busquedas.size()];
        for (int i = 0; i < busquedas.size(); i++) {
            arrayProvincias[i]=busquedas.get(i);
        }
        spinnerProvincias.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrayProvincias));
        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getApplicationContext(), VinoElegidoActivity.class);
                String idProvincia = spinnerProvincias.getSelectedItem().toString();
                mainActivity.putExtra("datoRecibido2", idProvincia);
                mainActivity.putExtra("datoRecibido1", mensaje2);
                startActivity(mainActivity);
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class GetVinos2 extends AsyncTask<String, String, String> {
        String elegido;
        @Override
        protected void onPreExecute() {
            busquedas.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;

            Statement st;
            ResultSet rs;

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select "+params[0]+"  from vinos group by "+params[0] +" order by nombre ");
                while (rs.next()) {
                    nombre = rs.getString(params[0]);
                    busquedas.add(nombre);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

                elegido = params[0];

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            rellenarDatosDeBusqueda(elegido);
        }
    }

    public class GetNombreBodegaVino extends AsyncTask<String, String, String> {
        String elegido;
        @Override
        protected void onPreExecute() {
            busquedas.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;
            int id = 0;

            Statement st;
            ResultSet rs;



            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select b.nombre from bodegas b inner join vinos v on v.bodega = b.id group by b.nombre order by b.nombre");
                while (rs.next()) {
                    nombre = rs.getString("b.nombre");
                    busquedas.add(nombre);

                }

                elegido = "bodega";

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            rellenarDatosDeBusqueda(elegido);
        }
    }
}
