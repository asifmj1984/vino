package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Controladores.RecyclerTouchListener;
import com.example.myapplication.Controladores.UsersAdapter;
import com.example.myapplication.Modelos.User;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.sql.ResultSet;
import java.util.ArrayList;

public class UserActivity extends AppCompatActivity {
    private RecyclerView rvUsers;
    UsersAdapter usersAdapter;

    ArrayList<User> users = new ArrayList<>();
    Button btnVerDetallesUsers;
    ImageView ivGetUser;
    String nombre;
    int idUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        btnVerDetallesUsers =  findViewById(R.id.btnAnyadirEnologo);
        GetNombreUser getUserName = new GetNombreUser();
        getUserName.execute("");
        idUser = getIntent().getExtras().getInt("idUser");
        nombre = getIntent().getExtras().getString("nombre");
        btnVerDetallesUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AnyadirUserActivity.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
            }
        });


        GetUsers getUsers = new GetUsers();
        getUsers.execute("");
    }


    public class GetUsers extends AsyncTask<String, String, String> {
        Connection con;
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            User user = null;
            String nombre = null;
            int id = 0;
            String email = null;
            String password = null;

            Statement st;
            ResultSet rs;


            try {

                con = (Connection) new ConnectionClass().CONN();

                st = (Statement) con.createStatement();

                rs = st.executeQuery("Select *  from users where id > 1");
                while (rs.next()) {
                    id = rs.getInt("id");
                    nombre = rs.getString("nombre");
                    email = rs.getString("email");
                    password = rs.getString("password");
                    user = new User(id, nombre, email, password);
                    users.add(user);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setUI();
        }
    }

    public class GetNombreUser extends AsyncTask<String, String, String> {
        Connection con;
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Statement st;
            ResultSet rs;


            try {

                con = (Connection) new ConnectionClass().CONN();

                st = (Statement) con.createStatement();

                rs = st.executeQuery("Select nombre  from users where id = "+idUser);
                while (rs.next()) {
                    nombre = rs.getString("nombre");
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

        }
    }

    public void setUI() {

        rvUsers = findViewById(R.id.rvEnologos);


        usersAdapter = new UsersAdapter(users);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        rvUsers.setLayoutManager(mLayoutManager);

        //rvRepartidores.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        rvUsers.setAdapter(usersAdapter);

        rvUsers.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvUsers, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                ivGetUser = view.findViewById(R.id.ivGetUser);
                ivGetUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), AnyadirUserActivity.class);
                        intent.putExtra("idUser", users.get(position).getId());
                        intent.putExtra("nombre", nombre);
                        startActivity(intent);
                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        usersAdapter.notifyDataSetChanged();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menhome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home1:
                Intent intent = new Intent(getApplicationContext(), MenuUserActivity.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
