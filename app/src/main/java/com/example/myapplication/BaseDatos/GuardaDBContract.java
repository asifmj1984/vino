package com.example.myapplication.BaseDatos;

import android.provider.BaseColumns;

public class GuardaDBContract {


    public static abstract class bodegasEntry  implements BaseColumns {
        public static final String TABLE_NAME ="TB_BODEGAS";

        public static final String ID = "ID";
        public static final String NOMBRE= "NOMBRE";
        public static final String DESCRIPCION = "DESCRIPCION";
        public static final String RUTAVINO= "RUTAVINO";
        public static final String LOGOTIPO = "LOGOTIPO";
    }

}
