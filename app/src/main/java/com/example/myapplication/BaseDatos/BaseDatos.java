package com.example.myapplication.BaseDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.example.myapplication.Modelos.Bodega;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Locale;


public class BaseDatos extends SQLiteOpenHelper {

    private Bodega bodega;

    private static BaseDatos sInstance;

    private static final String DATABASE_NAME = "bd_datos";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase db;
    public static synchronized BaseDatos getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new BaseDatos(context.getApplicationContext());
        }
        return sInstance;
    }

    public BaseDatos(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = this.getWritableDatabase();
    }


    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }


    public void limpiarBd() {
        getWritableDatabase().execSQL("DELETE FROM " + GuardaDBContract.bodegasEntry.TABLE_NAME);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + GuardaDBContract.bodegasEntry.TABLE_NAME + " ("
                + GuardaDBContract.bodegasEntry.ID + " INTEGER PRIMARY KEY,"
                + GuardaDBContract.bodegasEntry.NOMBRE + " TEXT NOT NULL,"
                + GuardaDBContract.bodegasEntry.DESCRIPCION + " TEXT NOT NULL,"
                + GuardaDBContract.bodegasEntry.RUTAVINO + " INTEGER NOT NULL,"
                + GuardaDBContract.bodegasEntry.LOGOTIPO + " BLOB NOT NULL,"
                + "UNIQUE (" + GuardaDBContract.bodegasEntry.ID + "))");

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + GuardaDBContract.bodegasEntry.TABLE_NAME);
        //Se crea la nueva versión de la tabla
        onCreate(db);
    }

    public void ejecutaQuery(String sql) {

        getWritableDatabase().execSQL(sql);
    }

    public boolean existeArticulo(int id) {
        boolean result = false;
        Cursor c = getReadableDatabase().rawQuery(
                String.format(Locale.getDefault(), " SELECT ID " +
                        "FROM TB_ARTICULOS " +
                        "WHERE ID = %d",  id),
                null);
        try {
            if (c.moveToFirst()) {
                result = true;
            }
        } finally {
            if (!c.isClosed())
                c.close();
        }
        return result;
    }


    //** DEVUELVE TODOS REGISTROS DE FORMAS PAGO EN UN ARRAYLIST **
    /*public ArrayList<Bodega> getBodegas(){
        //borrarTodasLasBodegas();
        //Creamos el cursor
        ArrayList<Bodega> listBodegas =new ArrayList<Bodega>();
        Cursor c = getWritableDatabase().rawQuery("select * from "+ GuardaDBContract.bodegasEntry.TABLE_NAME, null);
        if (c != null && c.getCount()>0) {
            c.moveToFirst();
            do {
                //Asignamos el valor en nuestras variables para crear un nuevo objeto
                int id = Integer.parseInt(c.getString(c.getColumnIndex(GuardaDBContract.bodegasEntry.ID)));
                String nombre = c.getString(c.getColumnIndex(GuardaDBContract.bodegasEntry.NOMBRE));
                String descripcion = c.getString(c.getColumnIndex(GuardaDBContract.bodegasEntry.DESCRIPCION));
                String rutaVino = c.getString(c.getColumnIndex(GuardaDBContract.bodegasEntry.RUTAVINO));
                ByteArrayOutputStream baos = new ByteArrayOutputStream(20480);
               // image.compress(Bitmap.CompressFormat.PNG, 0 , baos);
                byte[] blob = c.getBlob(c.getColumnIndex(GuardaDBContract.bodegasEntry.LOGOTIPO));

                bodega  =new Bodega(id, nombre, descripcion, rutaVino, blob);
                //Lo añadimos a la lista
                listBodegas.add(bodega);
            } while (c.moveToNext());
        }

        //Cerramos el cursor
        c.close();
        return listBodegas;
    }*/

    //** BORRA TODOS LOS REPARTIDORES **
    public void borrarTodasLasBodegas(){
        getWritableDatabase().execSQL("delete from "+ GuardaDBContract.bodegasEntry.TABLE_NAME);
    }

    public void insertaEnBodegas(int id, String nombre, String descripcion, int rutaVino, byte[] logotipo) {

        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(GuardaDBContract.bodegasEntry.ID, id);
        values.put(GuardaDBContract.bodegasEntry.NOMBRE, nombre);
        values.put(GuardaDBContract.bodegasEntry.DESCRIPCION, descripcion);
        values.put(GuardaDBContract.bodegasEntry.RUTAVINO,  rutaVino);
        values.put(GuardaDBContract.bodegasEntry.LOGOTIPO, logotipo);
        // Insertar...
        db.insert(GuardaDBContract.bodegasEntry.TABLE_NAME, null, values);

    }
}
