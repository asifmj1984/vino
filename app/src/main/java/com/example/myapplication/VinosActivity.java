package com.example.myapplication;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mysql.jdbc.Connection;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class VinosActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    ArrayList<String> busquedas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vinos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//este es el que cambia el color de fondo


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = "encuentratuvino.com";
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                startActivity(browserIntent);
            }
        });




    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.vinos, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        GetVinos task = new GetVinos();
        switch (item.getItemId()) {
            case R.id.action_settings:
                task.execute("nombre");
                return true;
            case R.id.action_buscarPorBodega:
                GetNombreBodegaVino task2 = new GetNombreBodegaVino();
                task2.execute("bodega");
                return true;
            case R.id.action_buscarPorAnio:
                task.execute("anyada");
                return true;
            case R.id.action_buscarPorDenomicacion:
                task.execute("denomicacionDeOrigen");
                return true;
            case R.id.action_buscarPorAlcohol:
                task.execute("alcohol");
                return true;
            case R.id.action_buscarPorVariante:
                task.execute("variantesDeUva");
                return true;
            case R.id.action_buscarPorPrecio:
                task.execute("precio");
                return true;
            case R.id.action_buscarEntrePrecios:
                rellenarDatosDePrecio();
                return true;

            case R.id.sign_in:
                rellenarDatosDeEmailYPass();
                return true;
            case R.id.homeVino:
                Intent mainActivity = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(mainActivity);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    private boolean etEstaVacio(EditText et){
        boolean comprobador;

        if(et.getText().toString().equals("")){
            comprobador= true;
            et.setBackgroundColor(getResources().getColor(R.color.rojopeligro));
        }else {
            comprobador = false;
            et.setBackgroundColor(getResources().getColor(R.color.whiteFF));
        }

        return  comprobador;
    }
    private void rellenarDatosDePrecio(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(VinosActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.dialog_buscar_entre_precios, null);
        builder.setView(v);
        final TextView tvTitulo = v.findViewById(R.id.tvDeBusquedaEntrePrecios);
        tvTitulo.setText(getApplicationContext().getText(R.string.elijamin));
        final Button btnAceptar = v.findViewById(R.id.btnAceptartEntrePrecios);
        final Button btncancelar = v.findViewById(R.id.btnCancelarEntrePrecios);
        final EditText minPrecio = v.findViewById(R.id.etMin);
        final EditText maxPrecio = v.findViewById(R.id.etMax);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(etEstaVacio(minPrecio)) && !(etEstaVacio(maxPrecio))){
                    Intent mainActivity = new Intent(getApplicationContext(), VinoElegidoActivity.class);

                    mainActivity.putExtra("precio1", minPrecio.getText().toString());
                    mainActivity.putExtra("precio2", maxPrecio.getText().toString());
                    System.out.println("precios                      " + minPrecio.getText().toString() +"        "+maxPrecio.getText().toString());
                    mainActivity.putExtra("datoRecibido1", "");
                    startActivity(mainActivity);
                    alertDialog[0].dismiss();
                }
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }
    private void rellenarDatosDeEmailYPass(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(VinosActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.activity_login_user, null);
        builder.setView(v);
        final Button btnAceptar = v.findViewById(R.id.btnAceptarLogin);
        final Button btncancelar = v.findViewById(R.id.btnCancelarLogin);
        final EditText etEmailUser = v.findViewById(R.id.etEmailUser);
        final EditText etPassUser = v.findViewById(R.id.etPassUser);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetUserData task3 = new GetUserData();
                String[] datos = new String[2];
                datos[0] = etEmailUser.getText().toString();
                datos[1] = etPassUser.getText().toString();
                task3.execute( datos);


                //alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    private void rellenarDatosDeBusqueda(final String mensaje2){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(VinosActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.dialog_buscar_por_provincia, null);
        builder.setView(v);
        final TextView tvTitulo = v.findViewById(R.id.tvDeBusqueda);
        tvTitulo.setText(getApplicationContext().getText(R.string.elija));
        final Button btnAceptar = v.findViewById(R.id.btnAceptartProvincia);
        final Button btncancelar = v.findViewById(R.id.btnCancelarProvincia);
        final Spinner spinnerProvincias = v.findViewById(R.id.spinnerProvincias);
        String [] arrayProvincias  = new String[busquedas.size()];
        for (int i = 0; i < busquedas.size(); i++) {
            arrayProvincias[i]=busquedas.get(i);
        }
        spinnerProvincias.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrayProvincias));
        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getApplicationContext(), VinoElegidoActivity.class);
                String idProvincia = spinnerProvincias.getSelectedItem().toString();
                mainActivity.putExtra("datoRecibido2", idProvincia);
                mainActivity.putExtra("datoRecibido1", mensaje2);
                startActivity(mainActivity);
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class GetVinos extends AsyncTask<String, String, String> {
        String elegido;
        @Override
        protected void onPreExecute() {
            busquedas.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;

            Statement st;
            ResultSet rs;

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select "+params[0]+"  from vinos group by "+params[0] +" order by nombre ");
                while (rs.next()) {
                    nombre = rs.getString(params[0]);
                    busquedas.add(nombre);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

                elegido = params[0];

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            rellenarDatosDeBusqueda(elegido);
        }
    }

    public class GetUserData extends AsyncTask<String[], String, String> {
        int id;
        String emailBueno;
        String passBueno;
        String nombre;
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String[]... params) {

            String response = "";
            Connection con;
            ResultSet rs;

            String[] credentials = params[0];
            String email = credentials[0];
            String password = credentials[1];

            try {

                con = (Connection) new ConnectionClass().CONN();

                String consulta = "select * from users where email = ?  and password  = ?";
                PreparedStatement ps = con.prepareStatement(consulta);
                ps.setString(1, email);
                ps.setString(2, password);
                rs = ps.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    emailBueno = rs.getString(2);
                    passBueno = rs.getString(3);
                    nombre  = rs.getString(4);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

          if(emailBueno != null){
             Intent intent = new Intent(getApplicationContext(), MenuUserActivity.class);
              intent.putExtra("idUser", id);
              intent.putExtra("nombre", nombre);
              startActivity(intent);
          }else{
              Toast toast1 = Toast.makeText(getApplicationContext(), getApplicationContext().getText(R.string.login_failed), Toast.LENGTH_SHORT);
              toast1.setGravity(Gravity.CENTER|Gravity.CENTER,0,0);
              toast1.show();
          }
        }
    }

    public class GetNombreBodegaVino extends AsyncTask<String, String, String> {
        String elegido;
        @Override
        protected void onPreExecute() {
            busquedas.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;
            int id = 0;

            Statement st;
            ResultSet rs;



            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select b.nombre from bodegas b inner join vinos v on v.bodega = b.id group by b.nombre order by b.nombre");
                while (rs.next()) {
                    nombre = rs.getString("b.nombre");
                    busquedas.add(nombre);

                }

                elegido = "bodega";

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            rellenarDatosDeBusqueda(elegido);
        }
    }

    @Override
    public void onBackPressed() {
        Intent mainActivity = new Intent(getApplicationContext(), PrincipalActivity.class);
        startActivity(mainActivity);
    }
}
