package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Controladores.BodegasAdapter;
import com.example.myapplication.Controladores.RecyclerTouchListener;
import com.example.myapplication.Modelos.Bodega;
import com.mysql.jdbc.Connection;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class BodegasUserActivity extends AppCompatActivity {
    private RecyclerView rvBodegas;
    BodegasAdapter bodegasAdapter;
    int idUser;
    String nombre;
    ArrayList<Bodega> bodegas = new ArrayList<>();
    Button btnVerDetallesBodega, btnAddBodega;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bodegas_user);
        idUser = getIntent().getExtras().getInt("idUser");
        nombre = getIntent().getExtras().getString("nombre");
        btnAddBodega = findViewById(R.id.btnAddBodega);
        btnAddBodega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AnyadirBodega.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                intent.putExtra("idBodega", 0);
                startActivity(intent);
            }
        });
        GetBodegas getBodegas = new GetBodegas();
        getBodegas.execute("");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menhome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home1:
                Intent intent = new Intent(getApplicationContext(), MenuUserActivity.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public class GetBodegas extends AsyncTask<String, String, String> {
        Connection con;
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";

            Bodega bodega = null;
            String nombre = null;
            int id = 0;
            String provincia = null;
            String breveDescripcion = null;
            String logotipo = null;
            String web = null;

            Statement st;
            ResultSet rs;


            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                    rs = st.executeQuery("Select *  from bodegas order by nombre");
                    while (rs.next()) {
                        id = rs.getInt("id");
                        nombre = rs.getString("nombre");
                        breveDescripcion = rs.getString("breveDescripcion");
                        provincia = rs.getString("provincia");
                        logotipo = rs.getString("logotipo");
                        web = rs.getString("web");
                        bodega = new Bodega(id, nombre, breveDescripcion, provincia, logotipo, web);
                        bodegas.add(bodega);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setUI();
        }
    }

    public void setUI() {

        rvBodegas = findViewById(R.id.rvBodegasUser);


        bodegasAdapter = new BodegasAdapter(bodegas, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        rvBodegas.setLayoutManager(mLayoutManager);

        //rvRepartidores.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        rvBodegas.setAdapter(bodegasAdapter);

        rvBodegas.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvBodegas, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                btnVerDetallesBodega = view.findViewById(R.id.btnVerDetallesBodegas);

                btnVerDetallesBodega.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), AnyadirBodega.class);
                        intent.putExtra("idUser", idUser);
                        intent.putExtra("nombre", nombre);
                        intent.putExtra("idBodega", bodegas.get(position).getId());
                        startActivity(intent);
                    }
                });


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        bodegasAdapter.notifyDataSetChanged();


    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MenuUserActivity.class);
        intent.putExtra("idUser", idUser);
        intent.putExtra("nombre", nombre);
        startActivity(intent);
    }
}
