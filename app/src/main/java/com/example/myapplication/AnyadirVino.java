package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Modelos.Bodega;
import com.example.myapplication.Modelos.Vino;
import com.mysql.jdbc.Connection;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AnyadirVino extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int SELECT_FILE = 1;
    ArrayList<String> bodegas = new ArrayList<>();
    Uri selectedImageUri;
    Spinner spinnerBodegas, spinnerTipo, spinnerFaseVisual, spinnerFaseBoca, spinnerFaseOlfativa, spinnerMaridaje;
    int serverResponseCode = 0;
    ProgressDialog dialog = null;
    EditText etNombreVino, etDenomincacionOrigen, etVarianteUva, etAnyada, etAlcohol, etPrecio,
            etMultilineTextObservaciones, etMultilineTextObservacionesMaridaje;
    TextView messageText, tvTituloVinoUsers;
    ImageView imageView;
    Button uploadButton,btnAnyadirVino, btnEliminarVino;
    int year , idUser, idVino;
    String nombre;
    String upLoadServerUri = null;
    Bitmap bitmap;
    String urlImage;
    /**********  File Path *************/
    String uploadFileName = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anyadir_vino);
        upLoadServerUri = "https://elorigendelvino.es/app/abc.php";
        uploadButton = findViewById(R.id.uploadButton);
        btnAnyadirVino = findViewById(R.id.btnAnyadirVino);
        spinnerBodegas = findViewById(R.id.spinnerAddBodega);
        spinnerTipo =  findViewById(R.id.spinnerAddTipo);
        spinnerFaseVisual = findViewById(R.id.spinnerVisual);
        spinnerFaseBoca = findViewById(R.id.spinnerBoca);
        spinnerFaseOlfativa = findViewById(R.id.spinnerOlfativa);
        spinnerMaridaje = findViewById(R.id.spinnerMaridaje);
        messageText  = findViewById(R.id.messageText);
        etNombreVino = findViewById(R.id.etNombreVino);
        etDenomincacionOrigen = findViewById(R.id.etDenomincacionOrigen);
        etVarianteUva = findViewById(R.id.etVarianteUva);
        etAnyada = findViewById(R.id.etAnyada);
        etAlcohol = findViewById(R.id.etAlcohol);
        etPrecio = findViewById(R.id.etPrecio);
        etMultilineTextObservaciones = findViewById(R.id.etMultilineTextObservaciones);
        etMultilineTextObservacionesMaridaje = findViewById(R.id.etMultilineTextObservacionesMaridaje);
        uploadFileName = "";
        imageView = findViewById(R.id.ivImagenElegida);
        tvTituloVinoUsers = findViewById(R.id.tvTituloVinoUsers);
        btnEliminarVino =  findViewById(R.id.btnEliminarVino);
        year = Calendar.getInstance().get(Calendar.YEAR);
        idUser = getIntent().getExtras().getInt("idUser");
        idVino= getIntent().getExtras().getInt("idVino");
        nombre = getIntent().getExtras().getString("nombre");
        rellenarSpinners();

        GetNombresBodegas taks = new GetNombresBodegas();
        taks.execute("");
        if(idUser == 1 && idVino !=0){
            btnEliminarVino.setVisibility(View.VISIBLE);
            btnEliminarVino.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deseaEliminar();
                }
            });
        }
        if(idVino != 0){
            tvTituloVinoUsers.setText(getResources().getText(R.string.modificarvino));
                    GetVino getVino = new GetVino();
                    getVino.execute("");
        }
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!checkPermission()) {
                    openActivity();
                } else {
                    if (checkPermission()) {
                        requestPermissionAndContinue();
                    } else {
                        openActivity();
                    }
                }
            }
        });
        if(idVino != 0){
            btnAnyadirVino.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if((!etEstaVacio(etNombreVino) && !etEstaVacio(etDenomincacionOrigen) && !etEstaVacio(etVarianteUva)
                            && !etEstaVacio(etAnyada) &&  !etEstaVacio(etPrecio) && !etEstaVacio(etAlcohol)
                            && !etEstaVacio(etMultilineTextObservaciones) && !etEstaVacio(etMultilineTextObservacionesMaridaje))){
                        dialog = ProgressDialog.show(AnyadirVino.this, "", "Uploading file...", true);

                        new Thread(new Runnable() {
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        messageText.setText("uploading started.....");
                                    }
                                });
                                uploadFile( uploadFileName);
                                UpdateVino updateVino = new UpdateVino();
                                updateVino.execute("");

                            }
                        }).start();
                    }

                }
            });
        }else{
            btnAnyadirVino.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if((!etEstaVacio(etNombreVino) && !etEstaVacio(etDenomincacionOrigen) && !etEstaVacio(etVarianteUva)
                            && !etEstaVacio(etAnyada) &&  !etEstaVacio(etPrecio) && !etEstaVacio(etAlcohol)
                            && !etEstaVacio(etMultilineTextObservaciones))  && !(tvEstaVacio(messageText)) && !(tvEstaVacio(etMultilineTextObservacionesMaridaje))){
                        dialog = ProgressDialog.show(AnyadirVino.this, "", "Uploading file...", true);

                        new Thread(new Runnable() {
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        messageText.setText("uploading started.....");
                                    }
                                });
                                uploadFile( uploadFileName);
                                Doregister register = new Doregister();
                                register.execute("");

                            }
                        }).start();
                    }

                }
            });
        }

    }
    private boolean checkPermission() {

        return ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ;
    }

    private void setAll(Vino vino){
            spinnerBodegas.setSelection(getStringOfSpinner(vino.getBodega(),spinnerBodegas));
            spinnerTipo.setSelection(getStringOfSpinner(vino.getTipo(),spinnerTipo));
            spinnerFaseVisual.setSelection(getStringOfSpinner(vino.getVisual()+"",spinnerFaseVisual));
            spinnerFaseBoca.setSelection(getStringOfSpinner(vino.getBoca()+"",spinnerFaseBoca));
            spinnerFaseOlfativa.setSelection(getStringOfSpinner(vino.getOlfativo()+"",spinnerFaseOlfativa));
            spinnerMaridaje.setSelection(getStringOfSpinner(vino.getMaridaje(),spinnerMaridaje));
            etNombreVino.setText(vino.getNombre());
            etDenomincacionOrigen.setText(vino.getDenomicacionDeOrigen());
            etVarianteUva.setText(vino.getVariantesDeUva());
            etAnyada.setText(vino.getAnyada());
            etAlcohol.setText(vino.getAlcohol()+"");
            etPrecio.setText(vino.getPrecio()+"");
            etMultilineTextObservaciones.setText(vino.getObservaciones());
            etMultilineTextObservacionesMaridaje.setText(vino.getObservacionesMaridaje());
            new GetImageFromUrl(imageView).execute(vino.getImagen());
            urlImage = vino.getImagen();
            btnAnyadirVino.setText("Modificar Vino");

    }

    private int getStringOfSpinner(String palabraBuscada, Spinner spinner){
        int palabra2 = 0;
        for (int i = 0; i <spinner.getCount() ; i++) {
            if(spinner.getItemAtPosition(i).toString().equals(palabraBuscada)){
                palabra2 = i;
            }
        }
        return palabra2;
    }

    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public GetImageFromUrl(ImageView img) {
            this.imageView = img;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
        }
    }

    private boolean etEstaVacio(EditText et){
        boolean comprobador;

        if(et.getText().toString().equals("")){
            comprobador= true;
            et.setBackgroundColor(getResources().getColor(R.color.rojopeligro));
        }else {
            comprobador = false;
            et.setBackgroundColor(getResources().getColor(R.color.whiteFF));
        }

        return  comprobador;
    }

    private boolean tvEstaVacio(TextView tv){
        boolean comprobador;

        if(tv.getText().toString().equals("")){
            comprobador= true;
            tv.setBackgroundColor(getResources().getColor(R.color.rojopeligro));
        }else {
            comprobador = false;
            tv.setBackgroundColor(getResources().getColor(R.color.whiteFF));
        }

        return  comprobador;
    }
    private void rellenarSpinnerBodegas(){
        String [] arrayBodegas  = new String[bodegas.size()];
        for (int i = 0; i < bodegas.size(); i++) {
            arrayBodegas[i]=bodegas.get(i);
        }
        spinnerBodegas.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrayBodegas));
    }
    private void rellenarSpinners(){
        String[] fases = getResources().getStringArray(R.array.fase);
        String[] tipos = getResources().getStringArray(R.array.tipo);
        String[] maridajes = getResources().getStringArray(R.array.maridaje);

        spinnerMaridaje.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,maridajes ));
        spinnerTipo.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,tipos ));
        spinnerFaseOlfativa.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,fases ));
        spinnerFaseBoca.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,fases ));
        spinnerFaseVisual.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,fases ));
    }

    private void requestPermissionAndContinue() {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle(("pipi"));
                alertBuilder.setMessage("pupuuuuuuu");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(AnyadirVino.this, new String[]{WRITE_EXTERNAL_STORAGE
                                , READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();
                Log.e("ffffffff", "permission denied, show dialog");
            } else {
                ActivityCompat.requestPermissions(AnyadirVino.this, new String[]{WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        } else {
            openActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    openActivity();
                } else {
                    finish();
                }

            } else {
                finish();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openActivity() {
        openGallery();
    }
    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API19(Context context, Uri uri){
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = { MediaStore.Images.Media.DATA };

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{ id }, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_FILE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == SELECT_FILE) {
                    selectedImageUri = data.getData();

                    uploadFileName = getRealPathFromURI_API19(getApplicationContext(),selectedImageUri);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    imageView.setImageBitmap(bitmap);

                    messageText.setText("Uploading file path "+uploadFileName+"");
                }
            }
        } catch (Exception e) {

        }
    }
    private String getNameOfUri(Uri contentURI) {

        String thePath = "no-path-found";
        String[] filePathColumn = {MediaStore.Images.Media.DISPLAY_NAME};
        Cursor cursor = getContentResolver().query(contentURI, filePathColumn, null, null, null);
        if(cursor.moveToFirst()){
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            thePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return  thePath;
    }
    @SuppressLint("LongLogTag")
    public int uploadFile(String sourceFileUri) {


        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {

            dialog.dismiss();

            Log.e("uploadFile", "Source File not exist :"+
                    uploadFileName);

            runOnUiThread(new Runnable() {
                public void run() {
                    messageText.setText("Source File not exist :"+
                            uploadFileName);
                }
            });

            return 0;

        }
        else
        {
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(upLoadServerUri);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if(serverResponseCode == 200){

                    runOnUiThread(new Runnable() {
                        public void run() {

                            /*String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
                                    + upLoadServerUri
                                    +pupu(selectedImageUri);

                            messageText.setText(msg);*/
                            Toast.makeText(AnyadirVino.this, "File Upload Complete.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {

                dialog.dismiss();
                ex.printStackTrace();

              /*  runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(AnyadirVino.this, "MalformedURLException",
                                Toast.LENGTH_SHORT).show();
                    }
                });*/

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {

                dialog.dismiss();
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("Got Exception : see logcat ");
                        Toast.makeText(AnyadirVino.this, "Got Exception : see logcat ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Upload file to server Exception", "Exception : "
                        + e.getMessage(), e);
            }
            dialog.dismiss();
            return serverResponseCode;

        } // End else block
    }

    public class Doregister extends AsyncTask<String, String, String> {

        String z = "";

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String nombre = etNombreVino.getText().toString();
            String bodega = spinnerBodegas.getSelectedItem().toString();
            String tipo = spinnerTipo.getSelectedItem().toString();
            String denOrigen = etDenomincacionOrigen.getText().toString();
            String varUva = etVarianteUva.getText().toString();
            String anyada = etAnyada.getText().toString();
            String alcohol = etAlcohol.getText().toString();
            String precio = etPrecio.getText().toString();
            String imagen = "https://elorigendelvino.es/app/images/"+ getNameOfUri(selectedImageUri);
            String creado = year +"";
            String maridaje = spinnerMaridaje.getSelectedItem().toString();
            String observaciones = etMultilineTextObservaciones.getText().toString();
            String observacionesMaridaje = etMultilineTextObservacionesMaridaje.getText().toString();
            String faseVisual = spinnerFaseVisual.getSelectedItem().toString();
            String faseBoca = spinnerFaseBoca.getSelectedItem().toString();
            String faseOlfativa = spinnerFaseOlfativa.getSelectedItem().toString();

            try {

                Connection con = (Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "INSERT INTO `vinos` (`id`, `nombre`, `bodega`,`tipo`, `denomicacionDeOrigen`, `variantesDeUva`," +
                        "`anyada`, `alcohol`, `precio`, `imagen`, `creado`, `maridaje1`, `observaciones`,  `idUser`, `observacionesMaridaje`) " +
                        "VALUES (NULL, '" + nombre + "', ( select id from bodegas where nombre =  '" + bodega + "' group by nombre)," +
                        " '"+tipo+"', '"+denOrigen+"', '"+varUva+"', '"+anyada+"', '"+alcohol+"', '"+precio+"', " +
                        " '"+imagen+"', '"+creado +"', '"+maridaje+"', '"+observaciones+"', '"+idUser+"', '"+observacionesMaridaje+ "');";
                Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                String query2 = "INSERT INTO `notas` (`id`, `idVino`, `faseVisual`, `faseOlfativa`, `faseBoca`) VALUES " +
                        " ( NULL, (SELECT id from vinos order by id DESC limit 1), '"+faseVisual+"', '"+faseOlfativa+"'," +
                        " '"+faseBoca+"');";
                Statement stmt2 = con.createStatement();
                stmt2.executeUpdate(query2);

                z = "Register successfull";

                System.out.println(" se ha regisradoooooooooooooooooooooooooooooooooooo");


            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menhome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home1:
                Intent intent = new Intent(getApplicationContext(), MenuUserActivity.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void volverActivity(){

        Intent intent = new Intent(getApplicationContext(), MenuUserActivity.class);
        intent.putExtra("idUser", idUser);
        intent.putExtra("nombre", nombre);
        startActivity(intent);
    }

    public class GetNombresBodegas extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;
            Statement st;
            ResultSet rs;


            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                    rs = st.executeQuery("Select nombre  from bodegas");
                    while (rs.next()) {
                        nombre = rs.getString("nombre");
                        bodegas.add(nombre);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {
            rellenarSpinnerBodegas();
        }
    }

    public class GetVino extends AsyncTask<String, String, String> {
        Vino vino;
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            com.mysql.jdbc.Connection con;

            String nombre = null;
            String nombreBodega = null;
            String tipo = null;
            String denomicacionDeOrigen = null;
            String variantesDeUva = null;
            String anyada = null;
            Double precio = null;
            Double alcohol = null;
            String imagen = null;
            String maridaje = null;
            String observaciones = null;
            String obervacionMaridaje = null;
            int visual;
            int boca;
            int olfativo;
            java.sql.Statement st;
            ResultSet rs;
            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                rs = st.executeQuery("Select v.*, b.nombre, n.*  from vinos v inner join bodegas b " +
                        "on b.id = v.bodega inner join notas n on v.id = n.idVino  where v.id = " + idVino);
                while (rs.next()) {
                    nombre = rs.getString("v.nombre");
                    nombreBodega = rs.getString("b.nombre");
                    tipo =  rs.getString("v.tipo");
                    denomicacionDeOrigen = rs.getString("v.denomicacionDeOrigen");
                    variantesDeUva = rs.getString("v.variantesDeUva");
                    anyada =  rs.getString("v.anyada");
                    alcohol = rs.getDouble("v.alcohol");
                    precio = rs.getDouble("v.precio");
                    imagen =  rs.getString("v.imagen");
                    visual =  rs.getInt("n.faseVisual");
                    boca =  rs.getInt("n.faseBoca");
                    olfativo =  rs.getInt("n.faseOlfativa");
                    maridaje = rs.getString("v.maridaje1");
                    observaciones = rs.getString("v.observaciones");
                    obervacionMaridaje = rs.getString("v.observacionesMaridaje");
                    vino =  new Vino (nombre, nombreBodega, tipo, denomicacionDeOrigen, variantesDeUva,
                            anyada,alcohol, precio, imagen, olfativo, boca, visual, observaciones, maridaje, obervacionMaridaje);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setAll(vino);
        }
    }

    public String devuelveReemplazado(String frase){
        return frase.replace("'","''");
    }

    public class UpdateVino extends AsyncTask<String, String, String> {


        String z = "";


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            String nombre = devuelveReemplazado(etNombreVino.getText().toString());
            String nombreBodega = spinnerBodegas.getSelectedItem().toString();
            String tipo = spinnerTipo.getSelectedItem().toString();
            String denomicacionDeOrigen = devuelveReemplazado(etDenomincacionOrigen.getText().toString());
            String variantesDeUva = devuelveReemplazado(etVarianteUva.getText().toString());
            String anyada = etAnyada.getText().toString();
            Double precio = Double.parseDouble(etPrecio.getText().toString());;
            Double alcohol = Double.parseDouble(etAlcohol.getText().toString());
            String obervacionMaridaje = devuelveReemplazado(etMultilineTextObservacionesMaridaje.getText().toString());
            String imagen;
            if(selectedImageUri == null){
                imagen = urlImage;
            }else{
                imagen = "https://elorigendelvino.es/app/images/"+ getNameOfUri(selectedImageUri);
            }
            String maridaje = spinnerMaridaje.getSelectedItem().toString();
            String observaciones = devuelveReemplazado(etMultilineTextObservaciones.getText().toString());
            int visual = Integer.valueOf(spinnerFaseVisual.getSelectedItem().toString()) ;
            int boca = Integer.valueOf(spinnerFaseBoca.getSelectedItem().toString()) ;
            int olfativo = Integer.valueOf(spinnerFaseOlfativa.getSelectedItem().toString()) ;
            try {

                Connection con = (Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "UPDATE vinos SET nombre ='" + nombre + "', bodega =  ( select id from bodegas " +
                        " where nombre ='" + nombreBodega + "' group by nombre), tipo = '"+tipo+"', " +
                        "denomicacionDeOrigen = '"+denomicacionDeOrigen+"', variantesDeUva = '"+variantesDeUva+"', " +
                        "anyada = '"+anyada+"', precio = '"+precio+"', alcohol = '"+alcohol+"', imagen = '"+imagen+"', " +
                        "maridaje1 = '"+maridaje+"', observaciones = '"+observaciones+"', idUser='"+idUser+"'," +
                        " observacionesMaridaje='"+obervacionMaridaje+"'  where id ='"+idVino+"' ;";
                Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                String query2 = "UPDATE notas SET faseVisual = '"+visual+"',faseBoca = '"+boca+"'," +
                        " faseOlfativa ='"+olfativo+"' WHERE idVino = '"+idVino+"'   ";
                Statement stmt2 = con.createStatement();
                stmt2.executeUpdate(query2);

                z = "Register successfull";

            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }

    private void deseaEliminar(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(AnyadirVino.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.cerrar_sesion, null);
        builder.setView(v);

        final TextView titulo = v.findViewById(R.id.tvTituloCerrarSesion);
        titulo.setText(getResources().getText(R.string.eliminar));
        final Button btnAceptar = v.findViewById(R.id.btnAceptarCerrarSesion);
        final Button btncancelar = v.findViewById(R.id.btnCancelarSesion);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteVino deleteVino= new DeleteVino();
                deleteVino.execute("");
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class DeleteVino extends AsyncTask<String, String, String> {


        String z = "";


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                com.mysql.jdbc.Connection con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "DELETE  from vinos where id ="+ idVino;
                java.sql.Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                z = "Register successfull";

            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss  delete    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        volverActivity();
    }
}
