package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class PrincipalActivity extends AppCompatActivity {

    ImageView irAMaridaje, irAJuego,irAVinos, irABodegas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_principal);

        irABodegas =findViewById(R.id.irABodegas);
        irAVinos = findViewById(R.id.btnVino);
        irAMaridaje = findViewById(R.id.btnMaridaje);
        irAJuego = findViewById(R.id.btnJuego);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            irABodegas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mainActivity = new Intent(getApplicationContext(), BodegasListaActivity.class);
                    mainActivity.putExtra("idBodega", 0);
                    startActivity(mainActivity);
                }
            });

            irAVinos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mainActivity = new Intent(getApplicationContext(), VinosActivity.class);
                    startActivity(mainActivity);
                }
            });

            irAMaridaje.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mainActivity = new Intent(getApplicationContext(), MaridajesActivity.class);
                    startActivity(mainActivity);
                }
            });

            irAJuego.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mainActivity = new Intent(getApplicationContext(), JuegoActivity.class);
                    startActivity(mainActivity);
                }
            });
        } else {
            Toast toast1 = Toast.makeText(getApplicationContext(), getApplicationContext().getText(R.string.nointernet), Toast.LENGTH_LONG);
            toast1.setGravity(Gravity.CENTER|Gravity.CENTER,0,0);
            toast1.show();
        }


    }
}
