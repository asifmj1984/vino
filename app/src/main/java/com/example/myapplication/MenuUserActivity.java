package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Controladores.RecyclerTouchListener;
import com.example.myapplication.Controladores.VinosAdapter;
import com.example.myapplication.Modelos.Vino;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MenuUserActivity extends AppCompatActivity {

    int idUser;
    String nombre;
    RecyclerView rvVinos;
    VinosAdapter vinosAdapter;
    ArrayList<Vino> vinos = new ArrayList<>();
    Button verDetalleVino,btnAddVIno,btnUserVerBodegas,btnUserVerUsers;
    TextView tvTituloAnyadir;
    ImageView imCerrarSesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_user);
        idUser = getIntent().getExtras().getInt("idUser");
        nombre = getIntent().getExtras().getString("nombre");
        btnAddVIno = findViewById(R.id.btnAddVIno);
        btnUserVerUsers = findViewById(R.id.btnUserVerUsers);
        btnUserVerBodegas = findViewById(R.id.btnUserVerBodegas);
        imCerrarSesion = findViewById(R.id.btnCerrarSesion);
        btnAddVIno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AnyadirVino.class);
                intent.putExtra("idVino",0);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
            }
        });
        tvTituloAnyadir = findViewById(R.id.tvTituloAnyadir);

        btnUserVerBodegas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BodegasUserActivity.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
            }
        });

        btnUserVerUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
            }
        });

        imCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deseaCerrarSesion();
            }
        });

        GetVinos task = new GetVinos();
        task.execute("");
        tvTituloAnyadir.setText(getResources().getText(R.string.welcome)+" "+nombre);
        if(idUser == 1){
            btnUserVerBodegas.setVisibility(View.VISIBLE);
            btnUserVerUsers.setVisibility(View.VISIBLE);
        }
    }

    public class GetVinos extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            com.mysql.jdbc.Connection con;

            Vino vino = null;
            int id = 0;
            String nombre = null;
            String imagen = null;
            String anyada = null;
            Double precio = null;
            java.sql.Statement st;
            ResultSet rs;
            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                if(idUser ==  1){
                    rs = st.executeQuery("Select v.*  from vinos v inner join bodegas b on v.bodega = b.id order by v.nombre ");
                    while (rs.next()) {
                        id = rs.getInt("id");
                        nombre = rs.getString("nombre");
                        imagen = rs.getString("imagen");
                        anyada = rs.getString("anyada");
                        precio = rs.getDouble("precio");
                        vino = new Vino(id, nombre, imagen, anyada, precio);
                        vinos.add(vino);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }
                }else{
                    rs = st.executeQuery("Select v.*  from vinos v inner join bodegas b on v.bodega = b.id  where v.idUser = '" + idUser +"'");
                    while (rs.next()) {
                        id = rs.getInt("id");
                        nombre = rs.getString("nombre");
                        imagen = rs.getString("imagen");
                        anyada = rs.getString("anyada");
                        precio = rs.getDouble("precio");
                        vino = new Vino(id, nombre, imagen, anyada, precio);
                        vinos.add(vino);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }
                }



            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setUI();
        }
    }

    //*** SE INICIAN LAS VARIABLES Y EL RECYCLER ***
    public void setUI() {

        rvVinos = findViewById(R.id.rvVinosUser);

        vinosAdapter = new VinosAdapter(vinos, this);

        GridLayoutManager mLayoutManager =   new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.VERTICAL, false);

        rvVinos.setLayoutManager(mLayoutManager);;

        rvVinos.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        rvVinos.setAdapter(vinosAdapter);

        rvVinos.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvVinos, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                verDetalleVino = view.findViewById(R.id.btnVerDetallesVino);

                verDetalleVino.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), AnyadirVino.class);
                        intent.putExtra("idVino", vinos.get(position).getId());
                        intent.putExtra("idUser", idUser);
                        intent.putExtra("nombre", nombre);
                        startActivity(intent);
                    }
                });


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        vinosAdapter.notifyDataSetChanged();

    }

    private void deseaCerrarSesion(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(MenuUserActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.cerrar_sesion, null);
        builder.setView(v);

        final Button btnAceptar = v.findViewById(R.id.btnAceptarCerrarSesion);
        final Button btncancelar = v.findViewById(R.id.btnCancelarSesion);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getApplicationContext(), VinosActivity.class);
                startActivity(mainActivity);
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    @Override
    public void onBackPressed() {
       //super.onBackPressed();
        deseaCerrarSesion();
    }
}
