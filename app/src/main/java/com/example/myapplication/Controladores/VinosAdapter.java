package com.example.myapplication.Controladores;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Modelos.Vino;
import com.example.myapplication.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

public class VinosAdapter extends RecyclerView.Adapter<VinosAdapter.PersonViewHolder> {

    List<Vino> vinos;
    private Context context;
    Bitmap bitmap;
    int total;
    String nameBodega;

    public VinosAdapter(List<Vino> vinos, Context context) {
        this.vinos = vinos;
        this.context = context;
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.detalle_vinos, parent, false);

        return new PersonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        final Vino vino = vinos.get(position);
        DecimalFormat df = new DecimalFormat("0.00");
        holder.vinoName.setText(vino.getNombre().toUpperCase());
        holder.vinoPrecio.setText(df.format(vino.getPrecio()) + " €");
        new GetNameBodega(holder.vinoAnyada).execute(vino.getId()+"");
        new GetImageFromUrl(holder.vinoImagen).execute(vino.getImagen());
        new GetRating(holder.ratingVinos, holder.vinoPrice).execute(vino.getId()+"");

       /* holder.irAWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = web;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                context.startActivity(browserIntent);
            }
        });*/
    }



    @Override
    public int getItemCount() {
        return vinos.size();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView vinoName;
        TextView vinoPrice;
        TextView vinoAnyada;
        TextView vinoPrecio;
        ImageView vinoImagen;
        RatingBar ratingVinos;
        // Button irAWeb;

        PersonViewHolder(View itemView) {
            super(itemView);
            vinoName = itemView.findViewById(R.id.tvNombreVino);
            vinoPrice = itemView.findViewById(R.id.tvPrecioVino);
            vinoPrecio = itemView.findViewById(R.id.tvPrecioDelVino);
            vinoAnyada = itemView.findViewById(R.id.tvAnyadaVino);
            vinoImagen = itemView.findViewById(R.id.ivVino);
            ratingVinos = itemView.findViewById(R.id.ratingBarVinos);

            //irAWeb = itemView.findViewById(R.id.btnVisitarWeb);
        }
    }

    public Bitmap stringToBitMap(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            System.out.println("Exceocipnm    " + e.getMessage());
            return null;
        }
    }

    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public GetImageFromUrl(ImageView img) {
            this.imageView = img;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
        }
    }


    public class GetRating extends AsyncTask<String, Void, Integer> {
        RatingBar ratingBar;
        TextView textView;
        public GetRating(RatingBar ratingBar,TextView textView) {
            this.ratingBar = ratingBar;
            this.textView = textView;
        }

        @Override
        protected Integer doInBackground(String... params) {
            com.mysql.jdbc.Connection con;

            int faseVisual = 0;
            int faseOlfactiva = 0;
            int faseBoca = 0;
            java.sql.Statement st;
            ResultSet rs;
            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                rs = st.executeQuery("Select *  from notas where idVino = " + params[0] +"");
                while (rs.next()) {
                    faseVisual = rs.getInt("faseVisual");
                    faseOlfactiva = rs.getInt("faseOlfativa");
                    faseBoca = rs.getInt("faseBoca");
                    total = ((faseVisual + faseOlfactiva + faseBoca)*50/30);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return total;


        }

        @Override
        protected void onPostExecute(Integer total) {
            super.onPostExecute(total);
            ratingBar.setRating((float) (total*(0.1)));
            textView.setText((float) (total*(0.1))+"");
        }
    }

    public class GetNameBodega extends AsyncTask<String, Void, String> {

        TextView textView;
        public GetNameBodega(TextView textView) {
            this.textView = textView;
        }

        @Override
        protected String doInBackground(String... params) {
            com.mysql.jdbc.Connection con;
            java.sql.Statement st;
            ResultSet rs;



            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                rs = st.executeQuery("Select b.nombre  from bodegas b inner join vinos v on v.bodega = b.id where v.id = " + params[0] +"");
                while (rs.next()) {
                    nameBodega = rs.getString("b.nombre");;
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return nameBodega;


        }

        @Override
        protected void onPostExecute(String nameBodega) {
            super.onPostExecute(nameBodega);
            textView.setText(nameBodega);
        }
    }
}