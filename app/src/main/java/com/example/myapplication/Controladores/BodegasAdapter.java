package com.example.myapplication.Controladores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Modelos.Bodega;
import com.example.myapplication.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class BodegasAdapter extends RecyclerView.Adapter<BodegasAdapter.PersonViewHolder> {

    List<Bodega> bodegas;
    private Context context;
    Bitmap bitmap;


    public BodegasAdapter(List<Bodega> bodegas,Context context) {
        this.bodegas = bodegas;
        this.context = context;
    }

    @NonNull
    @Override

    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.detalle_bodegas, parent, false);

        return new PersonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        final Bodega bodega = bodegas.get(position);
        holder.bodegaName.setText(bodega.getNombre());
        holder.bodegaDescripcion.setText(bodega.getProvincia());
        holder.bodegaRuta.setText(bodega.getBreveDescripcion());
            //holder.bodegaImagen.setImageBitmap(stringToBitMap(bodega.getLogotipo()));
        new GetImageFromUrl(holder.bodegaImagen).execute(bodega.getLogotipo());
        final String web = bodega.getWeb();

       /* holder.irAWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = web;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                context.startActivity(browserIntent);
            }
        });*/



    }

    @Override
    public int getItemCount() {
        return bodegas.size();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView bodegaName, bodegaDescripcion, bodegaRuta;
        ImageView bodegaImagen;
       // Button irAWeb;

        PersonViewHolder(View itemView) {
            super(itemView);
            bodegaName = (TextView) itemView.findViewById(R.id.tvNombre);
            bodegaDescripcion = (TextView) itemView.findViewById(R.id.tvDescripcion);
            bodegaRuta = (TextView) itemView.findViewById(R.id.tvRuta);
            bodegaImagen = (ImageView) itemView.findViewById(R.id.imBodega);
            //irAWeb = itemView.findViewById(R.id.btnVisitarWeb);
        }
    }

    public Bitmap stringToBitMap(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            System.out.println("Exceocipnm    "+ e.getMessage());
            return null;
        }
    }

    public  class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;
        public GetImageFromUrl(ImageView img){
            this.imageView = img;
        }
        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
        }
    }

}
