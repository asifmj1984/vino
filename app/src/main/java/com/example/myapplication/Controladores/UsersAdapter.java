package com.example.myapplication.Controladores;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Modelos.User;
import com.example.myapplication.R;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.PersonViewHolder> {

    List<User> users;

    public UsersAdapter(List<User> users) {
        this.users = users;
    }

    @NonNull
    @Override

    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.detaller_user, parent, false);

        return new PersonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        final User user = users.get(position);
        holder.userName.setText(user.getNombre());
        holder.userId.setText(user.getId()+"");
        holder.userPassword.setText(user.getPass());
        holder.userEmail.setText(user.getEmail());

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView userName;
        TextView userId;
        TextView userPassword;
        TextView userEmail;

        PersonViewHolder(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.tvNameUser);
            userId = (TextView) itemView.findViewById(R.id.tvIdUser);
            userPassword = (TextView) itemView.findViewById(R.id.tvPasswordUser);
            userEmail = (TextView) itemView.findViewById(R.id.tvEmailUser);

        }
    }
}