package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Modelos.Bodega;
import com.mysql.jdbc.Statement;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AnyadirBodega extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int SELECT_FILE_1 = 1;
    private static final int SELECT_FILE_2 = 2;
    private static final int SELECT_FILE_3 = 3;
    private static final int SELECT_FILE_4 = 4;

    ArrayList<String> provincias = new ArrayList<>();
    Bodega bodega;

    Uri selectedImageUri1, selectedImageUri2, selectedImageUri3, selectedImageUri4;
    Spinner spinnerProvincias;
    int serverResponseCode = 0;
    ProgressDialog dialog = null;
    EditText etNombreBodega, etBreveDescripcionBodega, etDescripcionBodega, etRutaVino,
            etDireccionBodega, etMunicipioBodega, etTelefono, etWeb, etEmailBodega;
    TextView messageText1, messageText2, messageText3, messageText4, tvTituloBodegasUser;
    ImageView imageView1, imageView2, imageView3, imageView4;
    Button uploadButton1, uploadButton2, uploadButton3, uploadButton4, btnAyadirBodega, btnEliminarBodega;
    int idBodega;
    String nombre;
    String upLoadServerUri = null;
    Bitmap bitmap1;
    String urlImage1, urlImage2, urlImage3, urlImage4;
    /**********  File Path *************/
    String uploadFileName1 = "";
    String uploadFileName2 = "";
    String uploadFileName3 = "";
    String uploadFileName4 = "";
    int idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anyadir_bodega);

        idBodega = getIntent().getExtras().getInt("idBodega");
        nombre = getIntent().getExtras().getString("nombre");
        idUser = getIntent().getExtras().getInt("idUser");
        upLoadServerUri = "https://elorigendelvino.es/app/abc.php";
        uploadButton1 = findViewById(R.id.uploadButton1);
        uploadButton2 = findViewById(R.id.uploadButton2);
        uploadButton3 = findViewById(R.id.uploadButton3);
        uploadButton4 = findViewById(R.id.uploadButton4);
        btnEliminarBodega = findViewById(R.id.btnEliminarBodega);

        messageText1 = findViewById(R.id.messageText1);
        messageText2 = findViewById(R.id.messageText2);
        messageText3 = findViewById(R.id.messageText3);
        messageText4 = findViewById(R.id.messageText4);
        tvTituloBodegasUser = findViewById(R.id.tvTituloBodegasUser);


        uploadFileName1 = "";
        uploadFileName3 = "";
        uploadFileName2 = "";
        uploadFileName4 = "";

        imageView1 = findViewById(R.id.ivImagenElegida1);
        imageView2 = findViewById(R.id.ivImagenElegida2);
        imageView3 = findViewById(R.id.ivImagenElegida3);
        imageView4 = findViewById(R.id.ivImagenElegida4);

        btnAyadirBodega = findViewById(R.id.btnAnyadirBodega);

        uploadButtonActivity(uploadButton1, SELECT_FILE_1);
        uploadButtonActivity(uploadButton2, SELECT_FILE_2);
        uploadButtonActivity(uploadButton3, SELECT_FILE_3);
        uploadButtonActivity(uploadButton4, SELECT_FILE_4);

        etNombreBodega = findViewById(R.id.etNombreBodega);
        etBreveDescripcionBodega = findViewById(R.id.etBreveDescripcionBodega);
        etDescripcionBodega = findViewById(R.id.etDescripcionBodega);
        etRutaVino = findViewById(R.id.etRutaVino);
        etDireccionBodega = findViewById(R.id.etDireccionBodega);
        etMunicipioBodega = findViewById(R.id.etMunicipioBodega);
        etTelefono = findViewById(R.id.etTelefono);
        etWeb = findViewById(R.id.etWeb);
        etEmailBodega = findViewById(R.id.etEmailBodega);

        spinnerProvincias = findViewById(R.id.spinnerAddProvincia);

        GetNombresProvincias getNombresProvincias = new GetNombresProvincias();
        getNombresProvincias.execute("");

        if (idBodega != 0) {
            tvTituloBodegasUser.setText(getResources().getText(R.string.moificarbodega));
            GetBodega getBodega = new GetBodega();
            getBodega.execute("");
            btnEliminarBodega.setVisibility(View.VISIBLE);
            btnEliminarBodega.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deseaEliminar();
                }
            });
        }

        if (idBodega != 0) {
            btnAyadirBodega.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((!etEstaVacio(etNombreBodega) && !etEstaVacio(etBreveDescripcionBodega) && !etEstaVacio(etDescripcionBodega)
                            && !etEstaVacio(etRutaVino) && !etEstaVacio(etDireccionBodega) && !etEstaVacio(etTelefono)
                            && !etEstaVacio(etWeb) && !etEstaVacio(etEmailBodega))) {
                        dialog = ProgressDialog.show(AnyadirBodega.this, "", "Uploading file...", true);

                        new Thread(new Runnable() {
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        messageText1.setText("uploading started.....");
                                    }
                                });
                                uploadFile(uploadFileName1, uploadFileName1, messageText1);
                                uploadFile(uploadFileName2, uploadFileName2, messageText2);
                                uploadFile(uploadFileName3, uploadFileName3, messageText3);
                                uploadFile(uploadFileName4, uploadFileName4, messageText4);
                                UpdateBodega updateVino = new UpdateBodega();
                                updateVino.execute("");

                            }
                        }).start();
                    }
                }
            });


        } else {
            btnAyadirBodega.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((!etEstaVacio(etNombreBodega) && !etEstaVacio(etBreveDescripcionBodega) && !etEstaVacio(etDescripcionBodega)
                            && !etEstaVacio(etRutaVino) && !etEstaVacio(etDireccionBodega) && !etEstaVacio(etTelefono)
                            && !etEstaVacio(etWeb) && !etEstaVacio(etEmailBodega)) && !(tvEstaVacio(messageText1))
                            && !(tvEstaVacio(messageText2)) && !(tvEstaVacio(messageText3)) && !(tvEstaVacio(messageText4))) {
                        dialog = ProgressDialog.show(AnyadirBodega.this, "", "Uploading file...", true);

                        new Thread(new Runnable() {
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        // messageText1.setText("uploading started.....");
                                    }
                                });
                                uploadFile(uploadFileName1, uploadFileName1, messageText1);
                                uploadFile(uploadFileName2, uploadFileName2, messageText2);
                                uploadFile(uploadFileName3, uploadFileName3, messageText3);
                                uploadFile(uploadFileName4, uploadFileName4, messageText4);
                                Doregister register = new Doregister();
                                register.execute("");

                            }
                        }).start();
                    }


                }
            });
        }


    }

    public String devuelveReemplazado(String frase){
        return frase.replace("'","''");
    }

    public class UpdateBodega extends AsyncTask<String, String, String> {

        String z = "";

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            String nombre = devuelveReemplazado(etNombreBodega.getText().toString());
            String rutaVino = devuelveReemplazado(etRutaVino.getText().toString());
            String breveDescripcion = devuelveReemplazado(etBreveDescripcionBodega.getText().toString());
            String descripcion = devuelveReemplazado(etDescripcionBodega.getText().toString());
            String direccion = devuelveReemplazado(etDireccionBodega.getText().toString());
            String municipio = devuelveReemplazado(etMunicipioBodega.getText().toString());
            String provincia = spinnerProvincias.getSelectedItem().toString();
            String telefono = devuelveReemplazado(etTelefono.getText().toString());
            String email = devuelveReemplazado(etEmailBodega.getText().toString());
            String web = devuelveReemplazado(etWeb.getText().toString());
            String logotipo;
            if (selectedImageUri1 == null) {
                logotipo = urlImage1;
            } else {
                logotipo = "https://elorigendelvino.es/app/images/" + getNameOfUri(selectedImageUri1);
            }
            String imagen1;
            if (selectedImageUri2 == null) {
                imagen1 = urlImage2;
            } else {
                imagen1 = "https://elorigendelvino.es/app/images/" + getNameOfUri(selectedImageUri2);
            }
            String imagen2;
            if (selectedImageUri3 == null) {
                imagen2 = urlImage3;
            } else {
                imagen2 = "https://elorigendelvino.es/app/images/" + getNameOfUri(selectedImageUri3);
            }
            String imagen3;
            if (selectedImageUri4 == null) {
                imagen3 = urlImage4;
            } else {
                imagen3 = "https://elorigendelvino.es/app/images/" + getNameOfUri(selectedImageUri4);
            }

            try {

                com.mysql.jdbc.Connection con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "UPDATE bodegas SET nombre ='" + nombre + "', breveDescripcion = " +
                        " '" + breveDescripcion + "' , rutaVino = '" + rutaVino + "', " +
                        "descripcion = '" + descripcion + "', direccion = '" + direccion + "', " +
                        "municipio = '" + municipio + "', provincia = '" + provincia + "', telefono = '" + telefono + "', email = '" + email + "', " +
                        "web = '" + web + "', logotipo = '" + logotipo + "',imagen1='" + imagen1 + "', imagen2='" + imagen2 + "', imagen3='" + imagen3 + "'   where id ='" + idBodega + "' ;";
                java.sql.Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                z = "Register successfull";

            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss  update    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }

    public class Doregister extends AsyncTask<String, String, String> {

        String z = "";

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String nombre = devuelveReemplazado(etNombreBodega.getText().toString());
            String rutaVino = devuelveReemplazado(etRutaVino.getText().toString());
            String breveDescripcion = devuelveReemplazado(etBreveDescripcionBodega.getText().toString());
            String descripcion = devuelveReemplazado(etDescripcionBodega.getText().toString());
            String logotipo = "https://elorigendelvino.es/app/images/" + getNameOfUri(selectedImageUri1);
            String direccion = devuelveReemplazado(etDireccionBodega.getText().toString());
            String municipio = devuelveReemplazado(etMunicipioBodega.getText().toString());
            String provincia = spinnerProvincias.getSelectedItem().toString();
            String telefono = devuelveReemplazado(etTelefono.getText().toString());
            String email = devuelveReemplazado(etEmailBodega.getText().toString());
            String web = devuelveReemplazado(etWeb.getText().toString());
            String imagen1 = "https://elorigendelvino.es/app/images/" + getNameOfUri(selectedImageUri2);
            String imagen2 = "https://elorigendelvino.es/app/images/" + getNameOfUri(selectedImageUri3);
            String imagen3 = "https://elorigendelvino.es/app/images/" + getNameOfUri(selectedImageUri4);
            ;


            try {

                com.mysql.jdbc.Connection con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "INSERT INTO `bodegas` (`id`, `nombre`, `breveDescripcion`,`descripcion`,`logotipo`, `rutaVino`, `direccion`," +
                        "`municipio`, `provincia`, `telefono`, `email`, `web`, `imagen1`, `imagen2`,  `imagen3`) " +
                        "VALUES (NULL, '" + nombre + "', '" + breveDescripcion + "'," +
                        " '" + descripcion + "', '" + logotipo + "', '" + rutaVino + "', '" + direccion + "', '" + municipio + "', '" + provincia + "', " +
                        " '" + telefono + "', '" + email + "', '" + web + "', '" + imagen1 + "', '" + imagen2 + "', '" + imagen3 + "');";
                java.sql.Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                z = "Register successfull";

                System.out.println(" se ha regisradoooooooooooooooooooooooooooooooooooo");


            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }

    private void volverActivity() {

        Intent intent = new Intent(getApplicationContext(), BodegasUserActivity.class);
        intent.putExtra("idUser", idUser);
        intent.putExtra("nombre", nombre);
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menhome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home1:
                Intent intent = new Intent(getApplicationContext(), MenuUserActivity.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void rellenarSpinnerProvincias() {

        String[] arrayProvincias = new String[provincias.size()];
        for (int i = 0; i < provincias.size(); i++) {
            arrayProvincias[i] = provincias.get(i);
        }
        spinnerProvincias.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrayProvincias));
    }

    public class GetNombresProvincias extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;
            Statement st;
            ResultSet rs;


            try {

                con = (Connection) new ConnectionClass().CONN();

                st = (Statement) con.createStatement();

                rs = st.executeQuery("Select provincia  from provincias");
                while (rs.next()) {
                    nombre = rs.getString("provincia");
                    provincias.add(nombre);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {
            rellenarSpinnerProvincias();
        }
    }

    private boolean etEstaVacio(EditText et) {
        boolean comprobador;

        if (et.getText().toString().equals("")) {
            comprobador = true;
            et.setBackgroundColor(getResources().getColor(R.color.rojopeligro));
        } else {
            comprobador = false;
            et.setBackgroundColor(getResources().getColor(R.color.whiteFF));
        }

        return comprobador;
    }

    private boolean tvEstaVacio(TextView tv) {
        boolean comprobador;

        if (tv.getText().toString().equals("")) {
            comprobador = true;
            tv.setBackgroundColor(getResources().getColor(R.color.rojopeligro));
        } else {
            comprobador = false;
            tv.setBackgroundColor(getResources().getColor(R.color.whiteFF));
        }

        return comprobador;
    }

    private void uploadButtonActivity(Button b, final int select_file) {
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkPermission()) {
                    openActivity(select_file);
                } else {
                    if (checkPermission()) {
                        requestPermissionAndContinue(select_file);
                    } else {
                        openActivity(select_file);
                    }
                }
            }
        });

    }

    public class GetBodega extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;
            int id = 0;
            String rutaVino = null;
            String breveDescripcion = null;
            String descripcion = null;
            String logotipo = null;
            String direccion = null;
            String municipio = null;
            String provincia = null;
            String telefono = null;
            String email = null;
            String web = null;
            String imagen1 = null;
            String imagen2 = null;
            String imagen3 = null;

            Statement st;
            ResultSet rs;

            try {

                con = new ConnectionClass().CONN();

                st = (Statement) con.createStatement();
                rs = st.executeQuery("Select *  from bodegas where id = " + idBodega);

                while (rs.next()) {
                    id = rs.getInt("id");
                    nombre = rs.getString("nombre");
                    breveDescripcion = rs.getString("breveDescripcion");
                    descripcion = rs.getString("descripcion");
                    rutaVino = rs.getString("rutaVino");
                    logotipo = rs.getString("logotipo");
                    direccion = rs.getString("direccion");
                    municipio = rs.getString("municipio");
                    provincia = rs.getString("provincia");
                    telefono = rs.getString("telefono");
                    email = rs.getString("email");
                    web = rs.getString("web");
                    imagen1 = rs.getString("imagen1");
                    imagen2 = rs.getString("imagen2");
                    imagen3 = rs.getString("imagen3");
                    bodega = new Bodega(id, nombre, breveDescripcion, descripcion, rutaVino, logotipo, direccion, municipio,
                            provincia, telefono, email, web, imagen1, imagen2, imagen3);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setAll();
        }
    }

    private boolean checkPermission() {

        return ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ;
    }

    private void setAll() {
        spinnerProvincias.setSelection(getStringOfSpinner(bodega.getProvincia(), spinnerProvincias));

        etNombreBodega.setText(bodega.getNombre());
        etBreveDescripcionBodega.setText(bodega.getBreveDescripcion());
        etDescripcionBodega.setText(bodega.getDescripcion());
        etRutaVino.setText(bodega.getRutaVino());
        etDireccionBodega.setText(bodega.getDireccion());
        etMunicipioBodega.setText(bodega.getMunicipio());
        etTelefono.setText(bodega.getTelefono());
        etWeb.setText(bodega.getWeb());
        etEmailBodega.setText(bodega.getEmail());

        new DownLoadImageTask(imageView1).execute(bodega.getLogotipo());
        // messageText1.setText("Uploading file path " + bodega.getLogotipo() + "");
        new DownLoadImageTask(imageView2).execute(bodega.getImagen1());
        new DownLoadImageTask(imageView3).execute(bodega.getImagen2());
        new DownLoadImageTask(imageView4).execute(bodega.getImagen3());

        urlImage1 = bodega.getLogotipo();
        urlImage2 = bodega.getImagen1();
        urlImage3 = bodega.getImagen2();
        urlImage4 = bodega.getImagen3();

        btnAyadirBodega.setText("Modificar Bodega");

    }

    private class DownLoadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public DownLoadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        /*
            doInBackground(Params... params)
                Override this method to perform a computation on a background thread.
         */
        protected Bitmap doInBackground(String... urls) {
            String urlOfImage = urls[0];
            Bitmap logo = null;
            try {
                InputStream is = new URL(urlOfImage).openStream();
                /*
                    decodeStream(InputStream is)
                        Decode an input stream into a bitmap.
                 */
                logo = BitmapFactory.decodeStream(is);
            } catch (Exception e) { // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        /*
            onPostExecute(Result result)
                Runs on the UI thread after doInBackground(Params...).
         */
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }
    }

    private void openActivity(int select_file) {
        openGallery(select_file);
    }

    private int getStringOfSpinner(String palabraBuscada, Spinner spinner) {
        int palabra2 = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(palabraBuscada)) {
                palabra2 = i;
            }
        }
        return palabra2;
    }

    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public GetImageFromUrl(ImageView img) {
            this.imageView = img;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap1 = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap1 = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap1;

        }
    }

    private void openGallery(int select_file) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Eliga una imagen"), select_file);
    }

    private void requestPermissionAndContinue(int select_file) {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setCancelable(true);
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(AnyadirBodega.this, new String[]{WRITE_EXTERNAL_STORAGE
                                , READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();
                Log.e("ffffffff", "permission denied, show dialog");
            } else {
                ActivityCompat.requestPermissions(AnyadirBodega.this, new String[]{WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        } else {
            openActivity(select_file);
        }
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    openActivity(requestCode);
                } else {
                    finish();
                }

            } else {
                finish();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == SELECT_FILE_1) {
                    selectedImageUri1 = data.getData();

                    uploadFileName1 = getRealPathFromURI_API19(getApplicationContext(), selectedImageUri1);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri1);
                    imageView1.setImageBitmap(bitmap);

                    messageText1.setText("Uploading file path " + uploadFileName1 + "");
                }
                if (requestCode == SELECT_FILE_2) {
                    selectedImageUri2 = data.getData();

                    uploadFileName2 = getRealPathFromURI_API19(getApplicationContext(), selectedImageUri2);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri2);
                    imageView2.setImageBitmap(bitmap);

                    messageText2.setText("Uploading file path " + uploadFileName2 + "");
                }
                if (requestCode == SELECT_FILE_3) {
                    selectedImageUri3 = data.getData();

                    uploadFileName3 = getRealPathFromURI_API19(getApplicationContext(), selectedImageUri3);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri3);
                    imageView3.setImageBitmap(bitmap);

                    messageText3.setText("Uploading file path " + uploadFileName3 + "");
                }
                if (requestCode == SELECT_FILE_4) {
                    selectedImageUri4 = data.getData();

                    uploadFileName4 = getRealPathFromURI_API19(getApplicationContext(), selectedImageUri4);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri4);
                    imageView4.setImageBitmap(bitmap);

                    messageText4.setText("Uploading file path " + uploadFileName4 + "");
                }
            }
        } catch (Exception e) {

        }
    }

    private String getNameOfUri(Uri contentURI) {

        String thePath = "no-path-found";
        String[] filePathColumn = {MediaStore.Images.Media.DISPLAY_NAME};
        Cursor cursor = getContentResolver().query(contentURI, filePathColumn, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            thePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return thePath;
    }

    @SuppressLint("LongLogTag")
    public int uploadFile(String sourceFileUri, final String uploadFileName, final TextView messageText) {


        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {

            dialog.dismiss();

            Log.e("uploadFile", "Source File not exist :" +
                    uploadFileName);

            runOnUiThread(new Runnable() {
                public void run() {
                    messageText.setText("Source File not exist :" +
                            uploadFileName);
                }
            });

            return 0;

        } else {
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(upLoadServerUri);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {

                            /*String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
                                    + upLoadServerUri
                                    +pupu(selectedImageUri);

                            messageText.setText(msg);*/
                            Toast.makeText(AnyadirBodega.this, "File Upload Complete.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {

                dialog.dismiss();
                ex.printStackTrace();

              /*  runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(AnyadirVino.this, "MalformedURLException",
                                Toast.LENGTH_SHORT).show();
                    }
                });*/

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {

                dialog.dismiss();
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("Got Exception : see logcat ");
                        Toast.makeText(AnyadirBodega.this, "Got Exception : see logcat ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Upload file to server Exception", "Exception : "
                        + e.getMessage(), e);
            }
            dialog.dismiss();
            return serverResponseCode;

        } // End else block
    }

    private void deseaEliminar() {
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(AnyadirBodega.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.cerrar_sesion, null);
        builder.setView(v);

        final TextView titulo = v.findViewById(R.id.tvTituloCerrarSesion);
        titulo.setText(getResources().getText(R.string.eliminar));
        final Button btnAceptar = v.findViewById(R.id.btnAceptarCerrarSesion);
        final Button btncancelar = v.findViewById(R.id.btnCancelarSesion);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteBodega deleteBodega = new DeleteBodega();
                deleteBodega.execute("");
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class DeleteBodega extends AsyncTask<String, String, String> {


        String z = "";


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                com.mysql.jdbc.Connection con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "DELETE  from bodegas where id =" + idBodega;
                java.sql.Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                z = "Register successfull";

            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss  delete    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        volverActivity();
    }
}
