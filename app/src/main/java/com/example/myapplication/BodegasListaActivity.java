package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Controladores.BodegasAdapter;
import com.example.myapplication.Controladores.RecyclerTouchListener;
import com.example.myapplication.Modelos.Bodega;
import com.example.myapplication.Modelos.Provincia;
import com.mysql.jdbc.Connection;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class BodegasListaActivity extends AppCompatActivity {
    private RecyclerView rvBodegas;
    BodegasAdapter bodegasAdapter;

    ArrayList<Bodega> bodegas = new ArrayList<>();
    ArrayList<Provincia> provincias = new ArrayList<>();
    String idBodegaProvincia;
    int idBodega;
    Button btnVerDetallesBodega;
    TextView tituloBodegas;
    TextView noBodegasProvincia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bodegas_lista);
        idBodegaProvincia = getIntent().getExtras().getString("idBodega2");
        idBodega = getIntent().getExtras().getInt("idBodega");

        TextView vistaFuente = (TextView) findViewById(R.id.tvBodegasTitulo);

//        Typeface face=Typeface.createFromAsset(getAssets(),"C:\\Users\\Asif\\Desktop\\project\\app\\src\\main\\res\\font\\idoniswash.ttf");
  //      vistaFuente.setTypeface(face);
        GetBodegas task = new GetBodegas();
        task.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mostrarProvinciasBodegas:
                GetProvincias task = new GetProvincias();
                task.execute();
                return true;
            case R.id.mostrarTodasLasBodegas:
                GetBodegas task2 = new GetBodegas();
                task2.execute();
                return true;
            case R.id.homeBodega:
                Intent mainActivity2 = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(mainActivity2);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


private void setTitulo(){
        tituloBodegas = findViewById(R.id.tvBodegasTitulo);
        noBodegasProvincia = findViewById(R.id.tvBodegasTitulo);
        if(idBodegaProvincia != null){
            tituloBodegas.setText("Bodegas de " + idBodegaProvincia);
        }else{
            tituloBodegas.setText(R.string.bodegas);
        }
    }
    //*** SE INICIAN LAS VARIABLES Y EL RECYCLER ***
    public void setUI() {

        rvBodegas = findViewById(R.id.rvBodegas);


        bodegasAdapter = new BodegasAdapter(bodegas, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        rvBodegas.setLayoutManager(mLayoutManager);

        //rvRepartidores.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        rvBodegas.setAdapter(bodegasAdapter);

        rvBodegas.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvBodegas, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                btnVerDetallesBodega = view.findViewById(R.id.btnVerDetallesBodegas);

                btnVerDetallesBodega.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), BodegaActivity.class);
                        //intent.putExtra("ResumenPedidos", baseDatos.devuelvePedidoBuscado(pedidosListSegunCriterio.get(Integer.parseInt(params[0])).getNumPedido()));
                        //intent.putExtra("ResumenPedidos", pedidosListSegunCriterio.get(Integer.parseInt(params[0])));
                        intent.putExtra("idBodega", bodegas.get(position).getId());
                        startActivity(intent);
                    }
                });


            }

            @Override
            public void onLongClick(View view,final int position) {


            }
        }));
        bodegasAdapter.notifyDataSetChanged();
        //setTitulo();

    }
    private void deseaEliminar(final int id){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(BodegasListaActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.cerrar_sesion, null);
        builder.setView(v);

        final TextView tvTituloCerrarSesion = v.findViewById(R.id.tvTituloCerrarSesion);
        final Button btnAceptar = v.findViewById(R.id.btnAceptarCerrarSesion);
        final Button btncancelar = v.findViewById(R.id.btnCancelarSesion);

        tvTituloCerrarSesion.setText("¿Seguro que deseas eliminarlo?");
        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteBodega deleteBodega = new DeleteBodega();
                deleteBodega.execute(String.valueOf(id));
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class GetBodegas extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;

            Bodega bodega = null;
            String nombre = null;
            int id = 0;
            String provincia = null;
            String breveDescripcion = null;
            String logotipo = null;
            String web = null;

            Statement st;
            ResultSet rs;

            //

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                if(idBodegaProvincia != null){
                    rs = st.executeQuery("Select *  from bodegas where provincia = '" + idBodegaProvincia+"' order by nombre");
                    while (rs.next()) {
                        id = rs.getInt("id");
                        nombre = rs.getString("nombre");
                        breveDescripcion = rs.getString("breveDescripcion");
                        provincia = rs.getString("provincia");
                        logotipo = rs.getString("logotipo");
                        web = rs.getString("web");
                        bodega = new Bodega(id, nombre, breveDescripcion, provincia, logotipo, web);
                        bodegas.add(bodega);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }
                }
                else {
                    rs = st.executeQuery("Select *  from bodegas order by nombre");
                    while (rs.next()) {
                        id = rs.getInt("id");
                        nombre = rs.getString("nombre");
                        breveDescripcion = rs.getString("breveDescripcion");
                        provincia = rs.getString("provincia");
                        logotipo = rs.getString("logotipo");
                        web = rs.getString("web");
                        bodega = new Bodega(id, nombre, breveDescripcion, provincia, logotipo, web);
                        bodegas.add(bodega);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }
                }



            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setUI();
        }
    }



    public byte[] convierteImagenABites(Bitmap bitmap) {
        // tamaño del baos depende del tamaño de tus imagenes en promedio
        ByteArrayOutputStream baos = new ByteArrayOutputStream(20480);
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, baos);
        byte[] blob = baos.toByteArray();
        return blob;

    }
    public class DeleteBodega extends AsyncTask<String, String, String> {

        String z = "";

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            try {

                com.mysql.jdbc.Connection con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = " DELETE FROM bodegas where id = '"+params[0]+"';";
                java.sql.Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                z = "Register successfull";

                System.out.println(" se ha eliminadoooooooo");


            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {

            setUI();
        }
    }


    private void rellenarDatosDeProvincia(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(BodegasListaActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.dialog_buscar_por_provincia, null);
        builder.setView(v);
        final Button btnAceptar = v.findViewById(R.id.btnAceptartProvincia);
        final Button btncancelar = v.findViewById(R.id.btnCancelarProvincia);
        final Spinner spinnerProvincias = v.findViewById(R.id.spinnerProvincias);
        String [] arrayProvincias  = new String[provincias.size()];
        for (int i = 0; i < provincias.size(); i++) {
            arrayProvincias[i]=provincias.get(i).getNombre();
        }
        spinnerProvincias.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrayProvincias));
        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getApplicationContext(), BodegasListaActivity.class);
                String idProvincia = spinnerProvincias.getSelectedItem().toString();
                mainActivity.putExtra("idBodega2", idProvincia);
                startActivity(mainActivity);
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class GetProvincias extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;

            Provincia provincia = null;
            String nombre = null;
            int id = 0;

            Statement st;
            ResultSet rs;

            //

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                    rs = st.executeQuery("Select provincia  from bodegas group by provincia");
                    while (rs.next()) {
                        nombre = rs.getString("provincia");
                        provincia = new Provincia(nombre);
                        provincias.add(provincia);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            rellenarDatosDeProvincia();
        }
    }

    @Override
    public void onBackPressed() {
        //new performBackgroundTask().execute();
        Intent intent = new Intent(getApplicationContext(), PrincipalActivity.class);
        startActivity(intent);
    }

}
