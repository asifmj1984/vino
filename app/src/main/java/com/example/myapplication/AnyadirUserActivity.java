package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Modelos.User;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.sql.ResultSet;

public class AnyadirUserActivity extends AppCompatActivity {


    User user ;
    int idUser;
    TextView tvTituloUser;
    String nombre;
    Button btnAnyadirUser, btnEliminarUser;

    EditText etPasswordUser, etEmailUser, etNombreUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anyadir_user);

        idUser = getIntent().getExtras().getInt("idUser");
        nombre = getIntent().getExtras().getString("nombre");
        btnAnyadirUser = findViewById(R.id.btnAnyadirEnologo);
        btnEliminarUser = findViewById(R.id.btnEliminarEnologo);
        tvTituloUser = findViewById(R.id.tvTituloUser);
        etNombreUser = findViewById(R.id.etNombreUser);
        etEmailUser = findViewById(R.id.etEmailUser);
        etPasswordUser = findViewById(R.id.etPasswordUser);

        if(idUser != 1){
            GetUsers getUsers = new GetUsers();
            getUsers.execute("");
            btnEliminarUser.setVisibility(View.VISIBLE);
            tvTituloUser.setText(getResources().getString(R.string.modificarenologo));
            btnAnyadirUser.setText(R.string.modificarenologo);
            btnAnyadirUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UpdateUser updateUser = new UpdateUser();
                    updateUser.execute("");
                }
            });
            btnEliminarUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deseaEliminar();
                }
            });

        }else{
            btnAnyadirUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Doregister doregister = new Doregister();
                    doregister.execute("");
                }
            });

        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menhome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home1:
                Intent intent = new Intent(getApplicationContext(), MenuUserActivity.class);
                intent.putExtra("idUser", 1);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void deseaEliminar(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(AnyadirUserActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.cerrar_sesion, null);
        builder.setView(v);

        final TextView titulo = v.findViewById(R.id.tvTituloCerrarSesion);
        titulo.setText(getResources().getText(R.string.eliminar));
        final Button btnAceptar = v.findViewById(R.id.btnAceptarCerrarSesion);
        final Button btncancelar = v.findViewById(R.id.btnCancelarSesion);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteUser deleteUser = new DeleteUser();
                deleteUser.execute("");
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class DeleteUser extends AsyncTask<String, String, String> {


        String z = "";


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                com.mysql.jdbc.Connection con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "DELETE  from users where id ="+ idUser;
                java.sql.Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                z = "Register successfull";

            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss  delete    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }

    public String devuelveReemplazado(String frase){
        return frase.replace("'","''");
    }

    public class UpdateUser extends AsyncTask<String, String, String> {
        String z = "";
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            String nombre = devuelveReemplazado(etNombreUser.getText().toString());
            String email = devuelveReemplazado(etEmailUser.getText().toString());
            String password = devuelveReemplazado(etPasswordUser.getText().toString());

            try {

                com.mysql.jdbc.Connection con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "UPDATE users SET nombre ='" + nombre + "', email = " +
                        " '" + email + "' , password = '" + password + "' " +
                        " where id =" + idUser + " ;";
                java.sql.Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                z = "Register successfull";

            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss  update    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }

    private void setAll(){
        etPasswordUser.setText(user.getPass()+"");
        etEmailUser.setText(user.getEmail());
        etNombreUser.setText(user.getNombre());
    }

    public class Doregister extends AsyncTask<String, String, String> {

        String z = "";

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String nombre = devuelveReemplazado(etNombreUser.getText().toString());
            String password = devuelveReemplazado(etPasswordUser.getText().toString());
            String email = devuelveReemplazado(etEmailUser.getText().toString());



            try {

                com.mysql.jdbc.Connection con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();
                // System.out.println("Database connection successssssssssssssss");
                String query = "INSERT INTO `users` (`id`, `nombre`,`email`, `password`) VALUES (NULL,  '"+
                        nombre+"', '"+email+"', '"+password+"');";
                java.sql.Statement stmt = con.createStatement();
                stmt.executeUpdate(query);

                z = "Register successfull";

                System.out.println(" se ha regisradoooooooooooooooooooooooooooooooooooo");


            } catch (Exception ex) {
                z = "Exceptions" + ex;
                System.out.println("exepcion essssssssssssssss    .   :   " + ex.getMessage());
            }

            return z;
        }


        @Override
        protected void onPostExecute(String s) {
            volverActivity();
        }
    }

    private void volverActivity() {

        Intent intent = new Intent(getApplicationContext(), UserActivity.class);
        intent.putExtra("idUser", 1);
        intent.putExtra("nombre", nombre);
        startActivity(intent);
    }

    public class GetUsers extends AsyncTask<String, String, String> {
        Connection con;
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            String nombre = null;
            int id = 0;
            String email = null;
            String password = null;

            Statement st;
            ResultSet rs = null;


            try {

                con = (Connection) new ConnectionClass().CONN();

                st = (Statement) con.createStatement();

                rs = st.executeQuery("Select *  from users where id = " + idUser + "");

                while (rs.next()) {
                    id = rs.getInt("id");
                    nombre = rs.getString("nombre");
                    email = rs.getString("email");
                    password = rs.getString("password");
                    user = new User(id, nombre, email, password);;
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {
            setAll();
        }
    }
}
