package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.mysql.jdbc.Connection;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class JuegoActivity extends AppCompatActivity {

    LinearLayout linearLayout;
    Button btnA, btnB, btnC, btnCTruncada, btnD, btnE, btnF, btnG, btnH, btnI, btnJ, btnK, btnL, btnM,
            btnN, btnEnie, btnO, btnP, btnQ, btnR, btnS, btnT, btnU, btnV, btnW, btnX, btnY, btnZ,
            btnComenzar, btnReiniciar, btnVerVinoJuego;
    ImageView im1, im2, im3, im4, im5, im6;
    int numeroAleatorio, maxLength;
    TextView tvIntentos, tvResultados;
    String nombreVino;
    ArrayList<Integer> ids = new ArrayList<>();
    int contador;
    int contadorVacio;
    EditText[] et;
    int idVino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);
        maxLength = 1;
        btnReiniciar = findViewById(R.id.btnReiniciar);
        btnComenzar = findViewById(R.id.btnComenzar);
        btnReiniciar.setEnabled(false);
        btnVerVinoJuego = findViewById(R.id.btnVerVinoJuego);
        btnVerVinoJuego.setVisibility(View.INVISIBLE);
        btnComenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                contador = 6;
                contadorVacio = 0;
                setUI();
                GetIdVinos getIdVinos = new GetIdVinos();
                getIdVinos.execute("");

                GetCountVinos getCountVinos = new GetCountVinos();
                getCountVinos.execute("");

                GetVinoAlAzar getVinoAlAzar = new GetVinoAlAzar();
                getVinoAlAzar.execute("");

                metodoPincharBoton();
                btnComenzar.setEnabled(false);
                btnReiniciar.setEnabled(true);
                btnVerVinoJuego.setVisibility(View.INVISIBLE);
            }
        });


        btnReiniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayout.removeAllViews();
                contador = 6;
                contadorVacio = 0;
                setUI();
                GetIdVinos getIdVinos = new GetIdVinos();
                getIdVinos.execute("");

                GetCountVinos getCountVinos = new GetCountVinos();
                getCountVinos.execute("");

                GetVinoAlAzar getVinoAlAzar = new GetVinoAlAzar();
                getVinoAlAzar.execute("");

                metodoPincharBoton();
                btnComenzar.setEnabled(false);
                btnReiniciar.setEnabled(true);
                btnVerVinoJuego.setVisibility(View.INVISIBLE);
                //enableOrDisableButton(true);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menhome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home1:
                Intent mainActivity = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(mainActivity);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setEnableButton(Button b){
        b.setEnabled(false);
    }

    private void metodoPincharBoton() {
        pincharButton(btnA);
        pincharButton(btnB);
        pincharButton(btnC);
        pincharButton(btnCTruncada);
        pincharButton(btnD);
        pincharButton(btnE);
        pincharButton(btnF);
        pincharButton(btnG);
        pincharButton(btnH);
        pincharButton(btnI);
        pincharButton(btnJ);
        pincharButton(btnK);
        pincharButton(btnL);
        pincharButton(btnM);
        pincharButton(btnN);
        pincharButton(btnEnie);
        pincharButton(btnO);
        pincharButton(btnP);
        pincharButton(btnQ);
        pincharButton(btnR);
        pincharButton(btnS);
        pincharButton(btnT);
        pincharButton(btnU);
        pincharButton(btnV);
        pincharButton(btnW);
        pincharButton(btnX);
        pincharButton(btnY);
        pincharButton(btnZ);
    }

    private void setVisibleImageView(int contadores){

        switch (contadores){
            case 5 :
                im6.setVisibility(View.VISIBLE);
                break;
            case 4 :
                im4.setVisibility(View.VISIBLE);
                break;
            case 3 :
                im2.setVisibility(View.VISIBLE);
                break;
            case 2 :
                im1.setVisibility(View.VISIBLE);
                break;
            case 1 :
                im3.setVisibility(View.VISIBLE);
                break;
            case 0 :
                im5.setVisibility(View.VISIBLE);
                break;
            default :

        }
    }


    private void setTextView() {
        char[] aCaracteres = nombreVino.toCharArray();
        et = new EditText[aCaracteres.length];
        for (int i = 0; i < aCaracteres.length; i++) {
            linearLayout = findViewById(R.id.layoutBuscado);
            EditText myEditText = new EditText(getApplicationContext());
            myEditText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            ColorStateList colorStateList = ColorStateList.valueOf(getResources().getColor(R.color.rojo));
            myEditText.setBackgroundTintList(colorStateList);

            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            myEditText.setFilters(FilterArray);
            myEditText.setText(aCaracteres[i] + "");
            myEditText.setEnabled(false);
            myEditText.setId(i);
            et[i] = myEditText;
            if (myEditText.getText().toString().equals(" ")) {
                myEditText.setVisibility(View.INVISIBLE);
                contadorVacio++;
            }
            et[i].setTag("libre");
            myEditText.setTextColor(getResources().getColor(R.color.whiteFF));
            linearLayout.addView(myEditText);
        }
        enableOrDisableButton(true);
    }

    private void setUI() {
        btnA = findViewById(R.id.btnA);
        btnB = findViewById(R.id.btnB);
        btnC = findViewById(R.id.btnC);
        btnCTruncada = findViewById(R.id.btnctrancada);
        btnD = findViewById(R.id.btnD);
        btnE = findViewById(R.id.btnE);
        btnF = findViewById(R.id.btnF);
        btnG = findViewById(R.id.btnG);
        btnH = findViewById(R.id.btnH);
        btnI = findViewById(R.id.btnI);
        btnJ = findViewById(R.id.btnJ);
        btnK = findViewById(R.id.btnK);
        btnL = findViewById(R.id.btnL);
        btnM = findViewById(R.id.btnM);
        btnN = findViewById(R.id.btnN);
        btnEnie = findViewById(R.id.btnEnie);
        btnO = findViewById(R.id.btnO);
        btnP = findViewById(R.id.btnP);
        btnQ = findViewById(R.id.btnQ);
        btnR = findViewById(R.id.btnR);
        btnS = findViewById(R.id.btnS);
        btnT = findViewById(R.id.btnT);
        btnU = findViewById(R.id.btnU);
        btnV = findViewById(R.id.btnV);
        btnW = findViewById(R.id.btnW);
        btnX = findViewById(R.id.btnX);
        btnY = findViewById(R.id.btnY);
        btnZ = findViewById(R.id.btnZ);



        im1 = findViewById(R.id.im1);
        im2 = findViewById(R.id.im2);
        im3 = findViewById(R.id.im3);
        im4 = findViewById(R.id.im4);
        im5 = findViewById(R.id.im5);
        im6 = findViewById(R.id.im6);

        im1.setVisibility(View.INVISIBLE);
        im2.setVisibility(View.INVISIBLE);
        im3.setVisibility(View.INVISIBLE);
        im4.setVisibility(View.INVISIBLE);
        im5.setVisibility(View.INVISIBLE);
        im6.setVisibility(View.INVISIBLE);


        tvIntentos = findViewById(R.id.tvIntentos);
        tvResultados = findViewById(R.id.tvResultados);
        tvResultados.setText("");
        tvIntentos.setText(""+contador);

        setEnableButton(btnA);
        setEnableButton(btnB);
        setEnableButton(btnC);
        setEnableButton(btnCTruncada);
        setEnableButton(btnD);
        setEnableButton(btnE);
        setEnableButton(btnF);
        setEnableButton(btnG);
        setEnableButton(btnH);
        setEnableButton(btnI);
        setEnableButton(btnJ);
        setEnableButton(btnK);
        setEnableButton(btnL);
        setEnableButton(btnM);
        setEnableButton(btnN);
        setEnableButton(btnEnie);
        setEnableButton(btnO);
        setEnableButton(btnP);
        setEnableButton(btnQ);
        setEnableButton(btnR);
        setEnableButton(btnS);
        setEnableButton(btnT);
        setEnableButton(btnU);
        setEnableButton(btnV);
        setEnableButton(btnW);
        setEnableButton(btnX);
        setEnableButton(btnY);
        setEnableButton(btnZ);
    }

    private void setResultado(){
        if (contadorVacio == et.length) {
            tvResultados.setText(getResources().getText(R.string.ganaste));
            btnVerVinoJuego.setVisibility(View.VISIBLE);
            enableOrDisableButton(false);
            btnVerVinoJuego.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GetIdVinoGanado getIdVinoGanado = new GetIdVinoGanado();
                    getIdVinoGanado.execute("");
                }
            });
        }
        if (contador == 0) {
            tvResultados.setText(getResources().getText(R.string.perdiste));
            enableOrDisableButton(false);
        }
    }

    private void enableOrDisableButton(boolean b){
        btnA.setEnabled(b);
        btnB.setEnabled(b);
        btnC.setEnabled(b);
        btnCTruncada.setEnabled(b);
        btnD.setEnabled(b);
        btnE.setEnabled(b);
        btnF.setEnabled(b);
        btnG.setEnabled(b);
        btnH.setEnabled(b);
        btnI.setEnabled(b);
        btnJ.setEnabled(b);
        btnK.setEnabled(b);
        btnL.setEnabled(b);
        btnM.setEnabled(b);
        btnN.setEnabled(b);
        btnEnie.setEnabled(b);
        btnO.setEnabled(b);
        btnP.setEnabled(b);
        btnQ.setEnabled(b);
        btnR.setEnabled(b);
        btnS.setEnabled(b);
        btnT.setEnabled(b);
        btnU.setEnabled(b);
        btnV.setEnabled(b);
        btnW.setEnabled(b);
        btnX.setEnabled(b);
        btnY.setEnabled(b);
        btnZ.setEnabled(b);
    }
    public class GetCountVinos extends AsyncTask<String, String, String> {

        int count;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;

            Statement st;
            ResultSet rs;

            //

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select count(*)  from vinos ");
                while (rs.next()) {
                    count = rs.getInt(1);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            numeroAleatorio = (int) (Math.random() * (ids.size()) + 1);
        }
    }

    public class GetIdVinos extends AsyncTask<String, String, String> {
        int id;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;

            Statement st;
            ResultSet rs;

            //

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select id  from vinos ");
                while (rs.next()) {
                    id = rs.getInt(1);
                    ids.add(id);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

        }
    }

    public class GetVinoAlAzar extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(JuegoActivity.this,R.style.AppCompatAlertDialogStyle);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(getResources().getString(R.string.espere));
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;

            Statement st;
            ResultSet rs;

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select nombre  from vinos where id = " + ids.get(numeroAleatorio));
                while (rs.next()) {
                    nombreVino = rs.getString(1);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }
                System.out.println("nombre                 erf   " + nombreVino);

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            setTextView();

        }
    }


    private void pincharButton(final Button b) {

        final int i = 0;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 int sumatorio = 0;

                    for (int i = 0; (i < et.length) ; i++) {
                        if (et[i].getText().toString().toUpperCase().equals(b.getText().toString())) {
                            et[i].setTextColor(getResources().getColor(R.color.design_default_color_error));
                            et[i].setTag("ocupado");
                            contadorVacio++;
                            sumatorio ++;
                            if(sumatorio==1){
                                contador++;
                            }

                        }
                    }
                contador--;
                tvIntentos.setText(""+contador);
                b.setEnabled(false);
                setResultado();
                setVisibleImageView(contador);
            }
        });

    }
    public class GetIdVinoGanado extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;

            Statement st;
            ResultSet rs;

            //

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select id  from vinos where nombre = '"+nombreVino+"'");
                while (rs.next()) {
                    idVino = rs.getInt(1);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {
            Intent intent = new Intent(getApplicationContext(), VinoDetalladoActivity.class);
            intent.putExtra("idVino", idVino);
            startActivity(intent);
        }
    }

}
