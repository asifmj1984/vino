package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Controladores.BodegasAdapter;
import com.example.myapplication.Controladores.RecyclerTouchListener;
import com.example.myapplication.Controladores.VinosBodegaAdapter;
import com.example.myapplication.Modelos.Bodega;
import com.example.myapplication.Modelos.Provincia;
import com.example.myapplication.Modelos.Vino;
import com.mysql.jdbc.Statement;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class BodegaActivity extends AppCompatActivity {

    int idBodega;
    Bodega bodega;
    private RecyclerView rvVinosBodega;
    TextView tvVinosRecomendados, nombreRecibido, descripcionRecibida;
    ArrayList<Vino> vinos = new ArrayList<>();
    Bitmap bitmap;
    VinosBodegaAdapter vinosBodegaAdapter;
    ArrayList<Provincia> provincias = new ArrayList<>();
    String idBodegaProvincia;

    ImageView ivImagenUnoBodega,ivImagenDosBodega, ivImagenTresBodega, ivLogotipoBodega,
            ivLlamarPorTelefonoBodega, irADireccionBodega,ivIrARutaVinoWebBodega, irAWebBodega,
    ivWeb1, ivWeb2, ivCompartir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bodega);


        idBodega = getIntent().getExtras().getInt("idBodega");

        GetBodega task = new GetBodega();
        task.execute();
        GetVinosBodegas task2 = new GetVinosBodegas();
        task2.execute();


    }

    private void setUI(){

        nombreRecibido = findViewById(R.id.tvNombreRecibido);
        descripcionRecibida = findViewById(R.id.tvNombreBodega);
        ivLogotipoBodega = findViewById(R.id.ivLogotipoBodega);
        tvVinosRecomendados = findViewById(R.id.tvVinosRecomendados);
        ivIrARutaVinoWebBodega = findViewById(R.id.ivIrARutaVinoWebBodega);
        irAWebBodega = findViewById(R.id.irAWebBodega);


        int anio = Calendar.getInstance().get(Calendar.YEAR);
        String recibido = tvVinosRecomendados.getText().toString();
        tvVinosRecomendados.setText(recibido + " "+anio);

        irADireccionBodega = findViewById(R.id.irADireccionBodega);
        ivLlamarPorTelefonoBodega = findViewById(R.id.ivLlamarPorTelefonoBodega);
        // ES EL OBJETO DE RESUMENPEDIDO RECIBIDO POR INTENT Y A PARTIR DE AHI SE SACAN TODOS LOS DATOS QUE SE NECESITAN

        String nombre = bodega.getNombre();
        String descripcion = bodega.getDescripcion();

        nombreRecibido.setText(descripcion);
        descripcionRecibida.setText(nombre);

        final String telefono2 = bodega.getTelefono();

        ivImagenUnoBodega = findViewById(R.id.ivImagenUnoBodega);

        ivImagenDosBodega = findViewById(R.id.ivImagenDosBodega);

        ivImagenTresBodega = findViewById(R.id.ivImagenTresBodega);
        new GetImageFromUrl(ivImagenTresBodega).execute(bodega.getImagen3());
        new GetImageFromUrl(ivImagenDosBodega).execute(bodega.getImagen2());
        new GetImageFromUrl(ivImagenUnoBodega).execute(bodega.getImagen1());
        new GetImageFromUrl(ivLogotipoBodega).execute(bodega.getLogotipo());



        ivWeb1 = findViewById(R.id.ivWeb3);
        ivWeb2 = findViewById(R.id.ivWeb4);
        ivCompartir = findViewById(R.id.ivCompartir1);

        ivWeb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "elorigendelvino.com";
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                startActivity(browserIntent);
            }
        });

        ivWeb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "encuentratuvino.com";
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                startActivity(browserIntent);
            }
        });

        ivCompartir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent compartir = new Intent(android.content.Intent.ACTION_SEND);
                compartir.setType("text/plain");
                String mensaje = "Te recomiendo esta App para encontrar los mejores vinos .\n https://encuentratuvino.com";
                compartir.putExtra(android.content.Intent.EXTRA_SUBJECT, "Vinos, Enoturismo");
                compartir.putExtra(android.content.Intent.EXTRA_TEXT, mensaje);
                startActivity(Intent.createChooser(compartir, "Compartir vía"));

            }
        });

        // BOTON PARA IR A MAPS DESDE LA APPLICACION CON LAS DIRECCIONES YA CALCULADAS
        irADireccionBodega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uri = "http://maps.google.com/maps?saddr=" + setLocation() + "&daddr=" + devuelveSegundaDireccion();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });

        // BOTON PARA HACER LLAMADA
        ivLlamarPorTelefonoBodega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dial = "tel:" + telefono2;
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
            }
        });

        irAWebBodega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = bodega.getWeb();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                startActivity(browserIntent);
            }
        });

        ivIrARutaVinoWebBodega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = bodega.getRutaVino();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                startActivity(browserIntent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mostrarProvinciasBodegas:
                GetProvincias task = new GetProvincias();
                task.execute();
                return true;
            case R.id.mostrarTodasLasBodegas:
                Intent mainActivity3 = new Intent(getApplicationContext(), BodegasListaActivity.class);
                startActivity(mainActivity3);
                finish();
                return true;
            case R.id.homeBodega:
                Intent mainActivity2 = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(mainActivity2);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void rellenarDatosDeProvincia(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(BodegaActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.dialog_buscar_por_provincia, null);
        builder.setView(v);
        final Button btnAceptar = v.findViewById(R.id.btnAceptartProvincia);
        final Button btncancelar = v.findViewById(R.id.btnCancelarProvincia);
        final Spinner spinnerProvincias = v.findViewById(R.id.spinnerProvincias);
        String [] arrayProvincias  = new String[provincias.size()];
        for (int i = 0; i < provincias.size(); i++) {
            arrayProvincias[i]=provincias.get(i).getNombre();
        }
        spinnerProvincias.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrayProvincias));
        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getApplicationContext(), BodegasListaActivity.class);
                String idProvincia = spinnerProvincias.getSelectedItem().toString();
                mainActivity.putExtra("idBodega2", idProvincia);
                startActivity(mainActivity);
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class GetProvincias extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            com.mysql.jdbc.Connection con;

            Provincia provincia = null;
            String nombre = null;
            int id = 0;

            java.sql.Statement st;
            ResultSet rs;

            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select provincia  from bodegas group by provincia");
                while (rs.next()) {
                    nombre = rs.getString("provincia");
                    provincia = new Provincia(nombre);
                    provincias.add(provincia);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            rellenarDatosDeProvincia();
        }
    }

    //*** DEVUELVE LA ULTIMA LOCALIZACION DEL DISPOSITIVO, SE HA UTILIZADO PARA NO TENER PROBLEMAS DE LOCALIZACIONES NULAS ***
    private Location getLastKnownLocation() {
        Location l = null;
        LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                l = mLocationManager.getLastKnownLocation(provider);
            }
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    //*** DEVUELVE LA DIRECCION DEL CLIENTE PARA A POSTERIORI UTILIZARLO EN EL MAPS ***
    private String devuelveSegundaDireccion() {

        String direccion = bodega.getDireccion();
        String municipio = bodega.getMunicipio();
        String provincia = bodega.getProvincia();
        return direccion + ",  " + municipio +", "+provincia;


    }

    //*** SE CONVIERTE A STRING NUESTA UBICACION ACTUAL PARA A POSTERIORI SER UTILIZADA EN EL GOOGLE MAPS ***
    public String setLocation() {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        String direccion = "";
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //String provider = locationManager.getBestProvider(new Criteria(), true);

        Location locations = getLastKnownLocation();
        List<String> providerList = locationManager.getAllProviders();
        if(null!=locations && null!=providerList && providerList.size()>0){
            double longitude = locations.getLongitude();
            double latitude = locations.getLatitude();
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if(null!=listAddresses && listAddresses.size()>0){
                    direccion = listAddresses.get(0).getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return  direccion;
    }

    public class GetBodega extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;
            int id = 0;
            String rutaVino = null;
            String descripcion = null;
            String logotipo = null;
            String direccion = null;
            String municipio = null;
            String provincia = null;
            String telefono = null;
            String email = null;
            String web = null;
            String imagen1 = null;
            String imagen2 = null;
            String imagen3 = null;

            Statement st;
            ResultSet rs;

            //

            try {

                con = new ConnectionClass().CONN();

                st = (Statement) con.createStatement();
                rs = st.executeQuery("Select *  from bodegas where id = " + idBodega);

                while (rs.next()) {
                    id = rs.getInt("id");
                    nombre = rs.getString("nombre");
                    descripcion = rs.getString("descripcion");
                    rutaVino = rs.getString("rutaVino");
                    logotipo = rs.getString("logotipo");
                    direccion = rs.getString("direccion");
                    municipio = rs.getString("municipio");
                    provincia = rs.getString("provincia");
                    telefono = rs.getString("telefono");
                    email = rs.getString("email");
                    web = rs.getString("web");
                    imagen1 = rs.getString("imagen1");
                    imagen2 = rs.getString("imagen2");
                    imagen3 = rs.getString("imagen3");
                    bodega = new Bodega(id, nombre, descripcion, rutaVino, logotipo, direccion, municipio,
                            provincia, telefono, email, web, imagen1, imagen2, imagen3);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setUI();
        }
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream ByteStream=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, ByteStream);
        byte [] b=ByteStream.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public Bitmap StringToBitMap(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;
        public GetImageFromUrl(ImageView img){
            this.imageView = img;
        }
        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
            final Drawable drawableImage1 = imageView.getDrawable();

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imagenComoPopUp(drawableImage1);
                }
            });
        }
    }

    //*** SE INICIAN LAS VARIABLES Y EL RECYCLER ***
    public void setRecycler() {

        rvVinosBodega = findViewById(R.id.rvVinosbodega);

        //repartidores = guardaDBHelper.getRepartidores();

        vinosBodegaAdapter = new VinosBodegaAdapter(vinos, this);

        GridLayoutManager  mLayoutManager =   new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);

        rvVinosBodega.setLayoutManager(mLayoutManager);

        //rvRepartidores.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        rvVinosBodega.setAdapter(vinosBodegaAdapter);

        rvVinosBodega.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvVinosBodega, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Intent intent = new Intent(getApplicationContext(), VinoDetalladoActivity.class);
                intent.putExtra("idVino", vinos.get(position).getId());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        vinosBodegaAdapter.notifyDataSetChanged();

    }

    public class GetVinosBodegas extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            com.mysql.jdbc.Connection con;

            Vino vino = null;
            int id = 0;
            String nombre = null;
            String imagen = null;
            String creado = null;
            java.sql.Statement st;
            ResultSet rs;
            int anio = Calendar.getInstance().get(Calendar.YEAR);
            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                    rs = st.executeQuery("Select v.id, v.nombre, v.imagen, v.creado, (n.faseVisual + n.faseOlfativa + faseBoca) as 'total' from vinos v" +
                            " inner join notas n on v.id = n.idVino where v.creado = " + anio +" and " +
                            "v.bodega = "+ idBodega +" order by total limit 4");
                    while (rs.next()) {
                        id = rs.getInt("v.id");
                        nombre = rs.getString("v.nombre");
                        imagen = rs.getString("v.imagen");
                        creado = rs.getString("v.creado");
                        vino = new Vino(id, nombre, imagen, creado);
                        vinos.add(vino);
                        //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                    }





            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setRecycler();
        }
    }

    private void imagenComoPopUp(Drawable drawable){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(BodegaActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.image, null);
        builder.setView(v);

        final ImageView ivImageBodegaSeleccionada = v.findViewById(R.id.ivImageBodegaSeleccionada);
        ivImageBodegaSeleccionada.setImageDrawable(drawable);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(true);

        alertDialog[0].show();
    }



}
