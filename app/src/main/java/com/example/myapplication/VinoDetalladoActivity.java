package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Modelos.Vino;
import com.mysql.jdbc.Connection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class VinoDetalladoActivity extends AppCompatActivity {
    int idVino, total;
    Float notaMedia;
    ArrayList<String> busquedas = new ArrayList<>();
    Vino vino;
    TextView tvVinosNombre, tvBodegaVino, tvTipoVino, tvAnyadaVino, tvDenominacionOrigen, tvAlcoholVino,
            tvVarianteUva, tvNotaMedia, tvOlfativa, tvVisual, tvBoca, tvObservacionesMaridaje, tvValoracionGeneral;
    ImageView ivVino, ivWeb1, ivWeb2, ivCompartir;
    Bitmap bitmap;

    RatingBar ratingBarVinoDetalladoMini;
    RatingBar ratingBarVinoDetallado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vino_detallado);
        idVino = getIntent().getExtras().getInt("idVino");
        new GetVino().execute("");
    }

    private void setUI() {
        tvVinosNombre = findViewById(R.id.tvVinoDetalladoNombre);
        tvBodegaVino = findViewById(R.id.tvVinoDetalladoBodega);
        tvTipoVino = findViewById(R.id.tvTipoVinoDetallado);
        tvAnyadaVino = findViewById(R.id.tvAnyadaVinoDetallado);
        tvDenominacionOrigen = findViewById(R.id.tvDenominacionOrigenDetallado);
        tvAlcoholVino = findViewById(R.id.tvAlcoholVinoDetallado);
        tvVarianteUva = findViewById(R.id.tvVinoVariedadDetallado);
        tvNotaMedia = findViewById(R.id.tvNotaMedia);
        tvOlfativa = findViewById(R.id.tvOfativaVino);
        tvVisual = findViewById(R.id.tvBocaVino);
        tvBoca = findViewById(R.id.tvVisualVino);
        ivVino = findViewById(R.id.ivVinoDetallado);
        tvValoracionGeneral =  findViewById(R.id.tvValoracionGeneral);
        tvObservacionesMaridaje = findViewById(R.id.tvObservacionesMaridaje);

        ivWeb1 = findViewById(R.id.ivWeb1);
        ivWeb2 = findViewById(R.id.ivWeb2);
        ivCompartir = findViewById(R.id.ivCompartir);

        ivWeb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "elorigendelvino.com";
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                startActivity(browserIntent);
            }
        });

        ivWeb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "encuentratuvino.com";
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+uri));
                startActivity(browserIntent);
            }
        });

        ivCompartir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mensaje = "";
                if(vino.getWeb() != null){
                     mensaje = "Te recomiendo esta App para encontrar vinos. \n "+vino.getWeb();
                }else{
                    mensaje = "Te recomiendo esta App para encontrar los mejores vinos .\n https://encuentratuvino.com";
                }

                Intent compartir = new Intent(android.content.Intent.ACTION_SEND);
                compartir.setType("text/plain");

                compartir.putExtra(android.content.Intent.EXTRA_SUBJECT, "Vino, enoturismo");
                compartir.putExtra(android.content.Intent.EXTRA_TEXT, mensaje);
                startActivity(Intent.createChooser(compartir, "Compartir vía"));

            }
        });

        ratingBarVinoDetallado = findViewById(R.id.ratingBarVinoDetallado);
        ratingBarVinoDetalladoMini = findViewById(R.id.ratingBarVinoDetalladoMini);


        notaMedia = (float)(((vino.getVisual() + vino.getOlfativo() + vino.getBoca())*50/30)*0.1);

        new GetImageFromUrl(ivVino).execute(vino.getImagen());

        Drawable progressDrawable = this.ratingBarVinoDetallado.getProgressDrawable();
        DrawableCompat.setTint(progressDrawable, ContextCompat.getColor(getApplicationContext(), R.color.rojobodega));

        Drawable progressDrawable2 = this.ratingBarVinoDetalladoMini.getProgressDrawable();
        DrawableCompat.setTint(progressDrawable2, ContextCompat.getColor(getApplicationContext(), R.color.rojobodega));

        ratingBarVinoDetallado.setRating((notaMedia));
        ratingBarVinoDetalladoMini.setRating((notaMedia));



        tvVinosNombre.setText(vino.getNombre().toUpperCase());
        tvBodegaVino.setText(vino.getBodega().toUpperCase());
        tvTipoVino.setText(vino.getTipo());
        tvAnyadaVino.setText(vino.getAnyada());
        tvDenominacionOrigen.setText(vino.getDenomicacionDeOrigen());
        tvAlcoholVino.setText(vino.getAlcohol()+" %");
        tvVarianteUva.setText(vino.getVariantesDeUva());
        tvNotaMedia.setText(notaMedia+"");
        tvOlfativa.setText(vino.getOlfativo()+"");
        tvVisual.setText(vino.getVisual()+"");
        tvBoca.setText(vino.getBoca()+"");
        tvValoracionGeneral.setText(vino.getObservaciones()+"");
        //tvValoracionGeneral.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        tvObservacionesMaridaje.setText(vino.getObservacionesMaridaje()+"");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dos, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        GetVinos2 task = new GetVinos2();
        switch (item.getItemId()) {
            case R.id.action_settings2:
                task.execute("nombre");
                return true;
            case R.id.action_buscarPorBodega2:
                GetNombreBodegaVino task2 = new GetNombreBodegaVino();
                task2.execute("bodega");
                return true;
            case R.id.action_buscarPorAnio2:
                task.execute("anyada");
                return true;
            case R.id.action_buscarPorDenomicacion2:
                task.execute("denomicacionDeOrigen");
                return true;
            case R.id.action_buscarPorAlcohol2:
                task.execute("alcohol");
                return true;
            case R.id.action_buscarPorVariante2:
                task.execute("variantesDeUva");
                return true;
            case R.id.action_buscarPorPrecio2:
                task.execute("precio");
                return true;
            case R.id.action_buscarEntrePrecios2:
                rellenarDatosDePrecio();
                return true;

            case R.id.homeVino2:
                Intent mainActivity = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(mainActivity);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean etEstaVacio(EditText et){
        boolean comprobador;

        if(et.getText().toString().equals("")){
            comprobador= true;
            et.setBackgroundColor(getResources().getColor(R.color.rojopeligro));
        }else {
            comprobador = false;
            et.setBackgroundColor(getResources().getColor(R.color.whiteFF));
        }

        return  comprobador;
    }
    private void rellenarDatosDePrecio(){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(VinoDetalladoActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.dialog_buscar_entre_precios, null);
        builder.setView(v);
        final TextView tvTitulo = v.findViewById(R.id.tvDeBusquedaEntrePrecios);
        tvTitulo.setText(getApplicationContext().getText(R.string.elijamin));
        final Button btnAceptar = v.findViewById(R.id.btnAceptartEntrePrecios);
        final Button btncancelar = v.findViewById(R.id.btnCancelarEntrePrecios);
        final EditText minPrecio = v.findViewById(R.id.etMin);
        final EditText maxPrecio = v.findViewById(R.id.etMax);

        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(etEstaVacio(minPrecio)) && !(etEstaVacio(maxPrecio))){
                    Intent mainActivity = new Intent(getApplicationContext(), VinoElegidoActivity.class);

                    mainActivity.putExtra("precio1", minPrecio.getText().toString());
                    mainActivity.putExtra("precio2", maxPrecio.getText().toString());
                    System.out.println("precios                      " + minPrecio.getText().toString() +"        "+maxPrecio.getText().toString());
                    mainActivity.putExtra("datoRecibido1", "");
                    startActivity(mainActivity);
                    alertDialog[0].dismiss();
                }

            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    private void rellenarDatosDeBusqueda(final String mensaje2){
        final AlertDialog[] alertDialog = new AlertDialog[1];
        final AlertDialog.Builder builder = new AlertDialog.Builder(VinoDetalladoActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        // SE INFLA LA VISTA DE ACEPTAR PARA LUEGO PODER ACCEDER A SUS BOTONES
        View v = inflater.inflate(R.layout.dialog_buscar_por_provincia, null);
        builder.setView(v);
        final TextView tvTitulo = v.findViewById(R.id.tvDeBusqueda);
        tvTitulo.setText(getApplicationContext().getText(R.string.elija));
        final Button btnAceptar = v.findViewById(R.id.btnAceptartProvincia);
        final Button btncancelar = v.findViewById(R.id.btnCancelarProvincia);
        final Spinner spinnerProvincias = v.findViewById(R.id.spinnerProvincias);
        String [] arrayProvincias  = new String[busquedas.size()];
        for (int i = 0; i < busquedas.size(); i++) {
            arrayProvincias[i]=busquedas.get(i);
        }
        spinnerProvincias.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrayProvincias));
        alertDialog[0] = builder.create();
        alertDialog[0].setCancelable(false);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getApplicationContext(), VinoElegidoActivity.class);
                String idProvincia = spinnerProvincias.getSelectedItem().toString();
                mainActivity.putExtra("datoRecibido2", idProvincia);
                mainActivity.putExtra("datoRecibido1", mensaje2);
                startActivity(mainActivity);
                alertDialog[0].dismiss();
            }
        });

        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog[0].dismiss();
            }
        });

        alertDialog[0].show();
    }

    public class GetVinos2 extends AsyncTask<String, String, String> {
        String elegido;
        @Override
        protected void onPreExecute() {
            busquedas.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;

            Statement st;
            ResultSet rs;

            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select "+params[0]+"  from vinos group by "+params[0] +" order by nombre ");
                while (rs.next()) {
                    nombre = rs.getString(params[0]);
                    busquedas.add(nombre);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

                elegido = params[0];

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            rellenarDatosDeBusqueda(elegido);
        }
    }

    public class GetNombreBodegaVino extends AsyncTask<String, String, String> {
        String elegido;
        @Override
        protected void onPreExecute() {
            busquedas.clear();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            Connection con;
            String nombre = null;
            int id = 0;

            Statement st;
            ResultSet rs;



            try {

                con = (Connection) new ConnectionClass().CONN();

                st = con.createStatement();

                rs = st.executeQuery("Select b.nombre from bodegas b inner join vinos v on v.bodega = b.id group by b.nombre order by b.nombre");
                while (rs.next()) {
                    nombre = rs.getString("b.nombre");
                    busquedas.add(nombre);

                }

                elegido = "bodega";

            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            rellenarDatosDeBusqueda(elegido);
        }
    }



    public class GetVino extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            com.mysql.jdbc.Connection con;

            String nombre = null;
            String nombreBodega = null;
            String tipo = null;
            String denomicacionDeOrigen = null;
            String variantesDeUva = null;
            String anyada = null;
            Double precio = null;
            Double alcohol = null;
            String imagen = null;
            int visual;
            int boca;
            int olfativo;
            String obervaciones;
            String obervacionesMaridaje;
            java.sql.Statement st;
            ResultSet rs;
            String maridaje = null;
            String web =  null;
            int anio = Calendar.getInstance().get(Calendar.YEAR);
            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                rs = st.executeQuery("Select v.*, b.nombre, n.*  from vinos v inner join bodegas b " +
                        "on b.id = v.bodega inner join notas n on v.id = n.idVino  where v.id = " + idVino);
                while (rs.next()) {
                    nombre = rs.getString("v.nombre");
                    nombreBodega = rs.getString("b.nombre");
                    tipo =  rs.getString("v.tipo");
                    denomicacionDeOrigen = rs.getString("v.denomicacionDeOrigen");
                    variantesDeUva = rs.getString("v.variantesDeUva");
                    anyada =  rs.getString("v.anyada");
                    alcohol = rs.getDouble("v.alcohol");
                    precio = rs.getDouble("v.precio");
                    imagen =  rs.getString("v.imagen");
                    visual =  rs.getInt("n.faseVisual");
                    boca =  rs.getInt("n.faseBoca");
                    olfativo =  rs.getInt("n.faseOlfativa");
                    maridaje = rs.getString("v.maridaje1");
                    obervaciones = rs.getString("v.observaciones");
                    obervacionesMaridaje = rs.getString("v.observacionesMaridaje");
                    web = rs.getString("v.web");
                    vino =  new Vino (nombre, nombreBodega, tipo, denomicacionDeOrigen, variantesDeUva,
                            anyada,alcohol, precio, imagen, olfativo, boca, visual,obervaciones,maridaje, obervacionesMaridaje, web);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {
            setUI();
        }
    }

    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public GetImageFromUrl(ImageView img) {
            this.imageView = img;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
        }
    }

    public class GetRating extends AsyncTask<String, Void, Integer> {
        RatingBar ratingBar;

        public GetRating(RatingBar ratingBar) {
            this.ratingBar = ratingBar;

        }

        @Override
        protected Integer doInBackground(String... params) {
            com.mysql.jdbc.Connection con;

            int faseVisual = 0;
            int faseOlfactiva = 0;
            int faseBoca = 0;
            java.sql.Statement st;
            ResultSet rs;
            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                rs = st.executeQuery("Select *  from notas where idVino = " + params[0] +"");
                while (rs.next()) {
                    faseVisual = rs.getInt("faseVisual");
                    faseOlfactiva = rs.getInt("faseOlfativa");
                    faseBoca = rs.getInt("faseBoca");
                    total = ((faseVisual + faseOlfactiva + faseBoca)*50/30);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            return total;


        }

        @Override
        protected void onPostExecute(Integer total) {
            super.onPostExecute(total);
            ratingBar.setRating((float) (total*(0.1)));

        }
    }

}
