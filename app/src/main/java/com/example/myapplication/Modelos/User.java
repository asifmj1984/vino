package com.example.myapplication.Modelos;

public class User {
    int id;
    String email;
    String pass;
    String nombre;

    public User(int id, String nombre, String email, String pass) {
        this.id = id;
        this.email = email;
        this.pass = pass;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPass() {
        return pass;
    }

    public String getNombre() {
        return nombre;
    }
}
