package com.example.myapplication.Modelos;

public class Bodega {

    private int id;
    private String nombre;
    private String descripcion;
    private String breveDescripcion;
    private String rutaVino;
    private String logotipo;
    private String direccion;
    private String municipio;
    private String provincia;
    private String telefono;
    private String email;
    private String web;
    private String imagen1;
    private String imagen2;
    private String imagen3;

    public Bodega() {

    }

    public String getBreveDescripcion() {
        return breveDescripcion;
    }

    public Bodega(int id, String nombre, String breveDescripcion, String provincia, String logotipo, String web) {
        this.id = id;
        this.nombre = nombre;
        this.breveDescripcion = breveDescripcion;
        this.provincia = provincia;
        this.logotipo = logotipo;
        this.web = web;
    }

    public Bodega(int id, String nombre, String breveDescripcion, String descripcion, String rutaVino, String logotipo, String
            direccion, String municipio, String provincia, String telefono, String email, String web,
                  String imagen1,String imagen2,String imagen3) {
        this.id = id;
        this.nombre = nombre;
        this.breveDescripcion = breveDescripcion;
        this.descripcion = descripcion;
        this.rutaVino = rutaVino;
        this.logotipo = logotipo;
        this.direccion =  direccion;
        this.municipio =  municipio;
        this.provincia = provincia;
        this.telefono = telefono;
        this.email =  email;
        this.web = web;
        this.imagen1 = imagen1;
        this.imagen2 = imagen2;
        this.imagen3 = imagen3;
    }

    public Bodega(int id, String nombre, String descripcion, String rutaVino, String logotipo, String
            direccion, String municipio, String provincia, String telefono, String email, String web,
                  String imagen1,String imagen2,String imagen3) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.rutaVino = rutaVino;
        this.logotipo = logotipo;
        this.direccion =  direccion;
        this.municipio =  municipio;
        this.provincia = provincia;
        this.telefono = telefono;
        this.email =  email;
        this.web = web;
        this.imagen1 = imagen1;
        this.imagen2 = imagen2;
        this.imagen3 = imagen3;
    }

    public String getLogotipo() {
        return logotipo;
    }

    public String getImagen1() {
        return imagen1;
    }

    public String getImagen2() {
        return imagen2;
    }

    public String getImagen3() {
        return imagen3;
    }

    public String getWeb() {
        return web;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRutaVino() {
        return rutaVino;
    }

    public void setRutaVino(String rutaVino) {
        this.rutaVino = rutaVino;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getMunicipio() {
        return municipio;
    }


}
