package com.example.myapplication.Modelos;

import java.time.Year;

public class Vino {
    private int id;
    private String nombre;
    private String bodega;
    private String tipo;
    private String denomicacionDeOrigen;
    private String variantesDeUva;
    private String anyada;
    private  Double alcohol;
    private Double precio;
    private String imagen;
    private String creado;
    private String observaciones;
    private String maridaje;
    private String observacionesMaridaje;
    private String web;
    int olfativo;
    int boca;
    int visual;

    public Vino(int id, String nombre, String imagen, String creado) {
        this.id = id;
        this.nombre = nombre;
        this.imagen = imagen;
        this.creado = creado;
    }

    public Vino( String nombre,String bodega, String imagen, String anyada, Double precio) {
        this.nombre = nombre;
        this.bodega = bodega;
        this.imagen = imagen;
        this.anyada = anyada;
        this.precio = precio;
    }

    public Vino(int id, String nombre, String imagen, String anyada, Double precio) {
        this.id = id;
        this.nombre = nombre;
        this.imagen = imagen;
        this.anyada = anyada;
        this.precio = precio;
    }

    public Vino(String nombre, String bodega, String tipo, String denomicacionDeOrigen, String variantesDeUva,
                String anyada, Double alcohol, Double precio, String imagen, int olfativo,int boca,int visual,
                String observaciones, String maridaje, String observacionesMaridaje , String web) {
        this.nombre = nombre;
        this.bodega = bodega;
        this.tipo = tipo;
        this.denomicacionDeOrigen = denomicacionDeOrigen;
        this.variantesDeUva = variantesDeUva;
        this.anyada = anyada;
        this.alcohol = alcohol;
        this.precio = precio;
        this.imagen = imagen;
        this.olfativo = olfativo;
        this.boca = boca;
        this.visual = visual;
        this.observaciones = observaciones;
        this.maridaje = maridaje;
        this.observacionesMaridaje = observacionesMaridaje;
        this.web = web;

    }

    public Vino(String nombre, String bodega, String tipo, String denomicacionDeOrigen, String variantesDeUva,
                String anyada, Double alcohol, Double precio, String imagen, int olfativo,int boca,int visual,
                String observaciones, String maridaje, String observacionesMaridaje ) {
        this.nombre = nombre;
        this.bodega = bodega;
        this.tipo = tipo;
        this.denomicacionDeOrigen = denomicacionDeOrigen;
        this.variantesDeUva = variantesDeUva;
        this.anyada = anyada;
        this.alcohol = alcohol;
        this.precio = precio;
        this.imagen = imagen;
        this.olfativo = olfativo;
        this.boca = boca;
        this.visual = visual;
        this.observaciones = observaciones;
        this.maridaje =  maridaje;
        this.observacionesMaridaje = observacionesMaridaje;
    }

    public String getMaridaje() {
        return maridaje;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public Double getAlcohol() {
        return alcohol;
    }

    public int getOlfativo() {
        return olfativo;
    }

    public int getBoca() {
        return boca;
    }

    public int getVisual() {
        return visual;
    }

    public String getTipo() {
        return tipo;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getBodega() {
        return bodega;
    }

    public String getDenomicacionDeOrigen() {
        return denomicacionDeOrigen;
    }

    public String getVariantesDeUva() {
        return variantesDeUva;
    }

    public String getAnyada() {
        return anyada;
    }

    public Double isAlcohol() {
        return alcohol;
    }


    public Double getPrecio() {
        return precio;
    }

    public String getImagen() {
        return imagen;
    }

    public String getCreado() {
        return creado;
    }

    public String getObservacionesMaridaje() {
        return observacionesMaridaje;
    }

    public String getWeb() {
        return web;
    }
}
