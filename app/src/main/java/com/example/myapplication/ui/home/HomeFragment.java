package com.example.myapplication.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.R;
import com.example.myapplication.VinoActivity;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    Button btnTinto, btnBlanco, btnRosado, bntEspumoso, btnDulce, btnEcologico;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        btnTinto = root.findViewById(R.id.btnTinto);
        btnBlanco = root.findViewById(R.id.btnBlanco);
        btnRosado = root.findViewById(R.id.btnRosado);
        bntEspumoso =  root.findViewById(R.id.btnEspumoso);
        btnDulce = root.findViewById(R.id.btnDulce);
        btnEcologico = root.findViewById(R.id.btnEcologico);

        goToIndicatedPage(btnTinto,getActivity().getString(R.string.tinto));
        goToIndicatedPage(btnBlanco, getActivity().getString(R.string.blanco));
        goToIndicatedPage(btnRosado, getActivity().getString(R.string.rosado));
        goToIndicatedPage(bntEspumoso, getActivity().getString(R.string.espumoso));
        goToIndicatedPage(btnDulce, getActivity().getString(R.string.dulce));
        goToIndicatedPage(btnEcologico, getActivity().getString(R.string.ecologico));

        return root;
    }

    private void goToIndicatedPage(Button btn, final String tipoVino){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getActivity(), VinoActivity.class);
                mainActivity.putExtra("tipo", tipoVino);
                startActivity(mainActivity);
            }
        });
    }
}
