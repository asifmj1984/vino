package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.BaseDatos.ConnectionClass;
import com.example.myapplication.Controladores.RecyclerTouchListener;
import com.example.myapplication.Controladores.VinosAdapter;
import com.example.myapplication.Modelos.Vino;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;

public class MaridajeActivity extends AppCompatActivity {
    ImageView ivTituloTipoVinoMaridaje;
    String tipoVino;
    RecyclerView rvVinos;
    VinosAdapter vinosAdapter;
    ArrayList<Vino> vinos = new ArrayList<>();
    Button verDetalleVino;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maridaje);
        tipoVino = getIntent().getExtras().getString("tipo");
        GetVinos taks = new GetVinos();
        taks.execute("");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menhome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home1:
                Intent mainActivity = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(mainActivity);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class GetVinos extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            com.mysql.jdbc.Connection con;

            Vino vino = null;
            int id = 0;
            String nombre = null;
            String imagen = null;
            String anyada = null;
            Double precio = null;
            java.sql.Statement st;
            ResultSet rs;
            int anio = Calendar.getInstance().get(Calendar.YEAR);
            //

            try {

                con = (com.mysql.jdbc.Connection) new ConnectionClass().CONN();

                st = con.createStatement();
                rs = st.executeQuery("Select *  from vinos where maridaje1 = '" + tipoVino + "' order by nombre");
                while (rs.next()) {
                    id = rs.getInt("id");
                    nombre = rs.getString("nombre");
                    imagen = rs.getString("imagen");
                    anyada = rs.getString("anyada");
                    precio = rs.getDouble("precio");
                    vino = new Vino(id, nombre, imagen, anyada, precio);
                    vinos.add(vino);
                    //baseDatos1.insertaEnBodegas(id, nombre, descripcion, rutaVino, logotipo);
                }





            } catch (Exception e) {
                e.printStackTrace();

            }
            return response;

        }


        @Override
        protected void onPostExecute(String s) {

            setUI();
        }
    }

    private void setTitulo(){
        ivTituloTipoVinoMaridaje = findViewById(R.id.ivTituloTipoVinoMaridaje);
        if(tipoVino.equals(getApplicationContext().getString(R.string.pollo))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajeconaves));
        }else if(tipoVino.equals(getApplicationContext().getString(R.string.pescado))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajeconpescado));
        }else if(tipoVino.equals(getApplicationContext().getString(R.string.ternera))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajeconternera));
        }else if(tipoVino.equals(getApplicationContext().getString(R.string.queso))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajesconqueso));
        }else if(tipoVino.equals(getApplicationContext().getString(R.string.cerdo))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajeconcerdo));
        }else if(tipoVino.equals(getApplicationContext().getString(R.string.pasta))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajeconpasta));
        }else if(tipoVino.equals(getApplicationContext().getString(R.string.cordero))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajeconcordero));
        }else if(tipoVino.equals(getApplicationContext().getString(R.string.entrantes))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajeconentrantes));
        } else if(tipoVino.equals(getApplicationContext().getString(R.string.ensaladas))){
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajeconensalada));
        }
        else{
            ivTituloTipoVinoMaridaje.setImageDrawable(getDrawable(R.drawable.maridajescondulce));
        }
    }
    //*** SE INICIAN LAS VARIABLES Y EL RECYCLER ***
    public void setUI() {

        rvVinos = findViewById(R.id.rvMaridajeVino);


        vinosAdapter = new VinosAdapter(vinos, this);

        GridLayoutManager mLayoutManager =   new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.VERTICAL, false);

        rvVinos.setLayoutManager(mLayoutManager);;

        //rvRepartidores.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        rvVinos.setAdapter(vinosAdapter);

        rvVinos.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvVinos, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                verDetalleVino = view.findViewById(R.id.btnVerDetallesVino);

                verDetalleVino.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), VinoDetalladoActivity.class);
                        intent.putExtra("idVino", vinos.get(position).getId());
                        startActivity(intent);
                    }
                });


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        vinosAdapter.notifyDataSetChanged();
        setTitulo();

    }
}
